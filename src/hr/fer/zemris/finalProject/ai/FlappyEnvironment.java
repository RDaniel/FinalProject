package hr.fer.zemris.finalProject.ai;

import hr.fer.zemris.finalProject.game.KeyManager;
import hr.fer.zemris.finalProject.game.Player;
import hr.fer.zemris.finalProject.game.flappy.FlappySimulator;
import hr.fer.zemris.finalProject.game.flappy.level.LevelGenerator;
import hr.fer.zemris.finalProject.game.flappy.level.MovingPipesGenerator;
import hr.fer.zemris.finalProject.game.flappy.level.NormalLevelGenerator;

public class FlappyEnvironment<T> implements Environment<T> {

	private static final int NUMBER_OF_ACTIONS = 2;
	private int numberOfInputs;
	private boolean movingPipes;

	private FlappySimulator simulator;

	private Player player;

	private KeyManager keyManager;

	private StateCreator<T> stateCreator;

	public FlappyEnvironment(StateCreator<T> stateCreator, boolean movingPipes) {
		this.stateCreator = stateCreator;
		this.movingPipes = movingPipes;

		numberOfInputs = movingPipes ? 4 : 3;
		keyManager = new KeyManager();
	}

	@Override
	public int getNumberOfActions() {
		return NUMBER_OF_ACTIONS;
	}

	@Override
	public int getNumberOfInputs() {
		return numberOfInputs;
	}

	@Override
	public T step(int action) {
		keyManager.setKey(action);
		int counter = 0;
		while (counter++ < 5) {
			simulator.gameStep();
		}

		player = simulator.getGameState().getPlayer();

		return stateCreator.getState(simulator.getGameState());
	}

	@Override
	public T initialize() {
		LevelGenerator levelGenerator;
		if (movingPipes) {
			levelGenerator = new MovingPipesGenerator(new NormalLevelGenerator());
		} else {
			levelGenerator = new NormalLevelGenerator();
		}

		simulator = new FlappySimulator(levelGenerator, keyManager, 0, movingPipes);
		player = simulator.getGameState().getPlayer();

		return stateCreator.getState(simulator.getGameState());
	}

	@Override
	public double getCurrentStateReward() {
		double reward = 0.1;
		if (player.isDead()) {
			reward = -1;
		}

		return reward;
	}

	@Override
	public boolean gameFinished() {
		return player.isDead();
	}

	@Override
	public int getFinalReward() {
		return simulator.getScore();
	}
}