package hr.fer.zemris.finalProject.ai;

import hr.fer.zemris.finalProject.game.flappy.util.RandomGenerator;

import java.util.ArrayList;
import java.util.List;

public class SARSATableLearner {

	private static final int NUMBER_OF_GAME_ITERATIONS = 500;

	private double learningRate;
	private double discountFactor;
	private int numberOfIterations;
	private double epsilonProbability;

	private double[][] Q;

	private Environment<Integer> environment;
	private List<SolutionListener> solutionListeners;

	public SARSATableLearner(Environment<Integer> environment, int numberOfIterations, double epsilonProbability, double learningRate, double discountFactor) {
		this.environment = environment;
		this.numberOfIterations = numberOfIterations;
		this.epsilonProbability = epsilonProbability;
		this.learningRate = learningRate;
		this.discountFactor = discountFactor;
		Q = new double[environment.getNumberOfInputs()][environment.getNumberOfActions()];
		solutionListeners = new ArrayList<>();
	}

	public double[][] getQ() {
		return Q;
	}

	public void learn() {
		for (int i = 0; i < numberOfIterations; i++) {
			int state = environment.initialize();
			double totalReward = 0;
			int action = getNextAction(state);
			for (int j = 0; j < NUMBER_OF_GAME_ITERATIONS; j++) {
				int nextState = environment.step(action);
				double reward = environment.getCurrentStateReward();

				totalReward += reward;

				int nextAction = getNextAction(nextState);
				Q[state][action] += learningRate * (reward + discountFactor * Q[nextState][nextAction] - Q[state][action]);

				if (environment.gameFinished()) {
					break;
				}

				state = nextState;
				action = nextAction;
			}

			for (SolutionListener listener : solutionListeners) {
				listener.solutionGenerated(i, totalReward);
			}
		}
	}

	public void addSolutionListener(SolutionListener solutionListener) {
		solutionListeners.add(solutionListener);
	}

	public void removeSolutionListner(SolutionListener solutionListener) {
		solutionListeners.add(solutionListener);
	}

	private int getMaxAction(double[] values) {
		double maximum = -Double.MAX_VALUE;
		int index = -1;
		for (int i = 0; i < values.length; i++) {
			if (values[i] > maximum) {
				maximum = values[i];
				index = i;
			}
		}

		return index;
	}

	private int getNextAction(int state) {
		if (RandomGenerator.getRandomDouble() < epsilonProbability) {
			return RandomGenerator.getRandomInteger(0, environment.getNumberOfActions() - 1);
		}

		return getMaxAction(Q[state]);
	}
}