package hr.fer.zemris.finalProject.ai;

import hr.fer.zemris.finalProject.ai.neuralNetwork.FeedForwardNeuralNetwork;
import hr.fer.zemris.finalProject.ai.neuralNetwork.NeuralArchitecture;
import hr.fer.zemris.finalProject.ai.neuralNetwork.NeuralNetwork;
import hr.fer.zemris.finalProject.ai.neuralNetwork.functions.*;
import hr.fer.zemris.finalProject.game.flappy.util.RandomGenerator;

import java.util.Arrays;

public class SARSANeuralNetworkLearner {

	private static final long NUMBER_OF_ITERATIONS = 100000000;
	private static final int NUMBER_OF_GAME_ITERATIONS = 500000;
	private static final double DISCOUNT_FACTOR = 0.8;

	private NeuralNetwork neuralNetwork;

	private Environment<double[]> environment;

	private LossFunction lossFunction;

	private int numberOfActions;

	private double[] maxWeights;

	public SARSANeuralNetworkLearner(Environment<double[]> environment) {
		this.environment = environment;
		numberOfActions = environment.getNumberOfActions();

		int[] layout = new int[]{environment.getNumberOfInputs(), 20, numberOfActions};
		ActivationFunction[] functions = new ActivationFunction[]{
				new IdentityFunction(),
				new ReLUFunction(),
				new IdentityFunction()
		};
		NeuralArchitecture architecture = new NeuralArchitecture(layout, functions);
		neuralNetwork = new FeedForwardNeuralNetwork(architecture);

		neuralNetwork = new FeedForwardNeuralNetwork(architecture);
		neuralNetwork.setNetworkWeights(neuralNetwork.getNetworkWeights());

		lossFunction = new SquaredSumLossFunction();
	}

	public NeuralNetwork learn() {
		double maxRew = -Double.MAX_VALUE;
		for (long i = 0; i < NUMBER_OF_ITERATIONS; i++) {
			double[] state = environment.initialize();
			double rewards = 0;
			int j;
			double[] qValues = neuralNetwork.forwardPass(state);
			int action = epsilonGreedActionSelection(qValues, i);
			for (j = 0; j < NUMBER_OF_GAME_ITERATIONS; j++) {
				double[] nextState = environment.step(action);
				double[] nextQValues = neuralNetwork.forwardPass(nextState);
				int nextAction = epsilonGreedActionSelection(nextQValues, i);
				double reward = environment.getCurrentStateReward();
				rewards += reward;

				double[] targetQValues = Arrays.copyOf(qValues, qValues.length);
				targetQValues[nextAction] = reward + DISCOUNT_FACTOR * nextQValues[nextAction];

				neuralNetwork.updateValues(lossFunction.calculateGradients(qValues, targetQValues));

				if (environment.gameFinished()) {
					break;
				}

				action = nextAction;
			}

			if (rewards > maxRew) {
				System.out.println(environment.getFinalReward());
				maxRew = rewards;
				maxWeights = neuralNetwork.getNetworkWeights();
			}

		}

		neuralNetwork.setNetworkWeights(maxWeights);
		return neuralNetwork;
	}

	private int getMaxRewardAction(double[] values) {
		double maximum = -Double.MAX_VALUE;
		int index = -1;
		for (int i = 0; i < values.length; i++) {
			if (values[i] > maximum) {
				maximum = values[i];
				index = i;
			}
		}

		return index;
	}

	private int epsilonGreedActionSelection(double[] values, long currentIteration) {
		double probability = 0.02 * (NUMBER_OF_ITERATIONS - currentIteration) / (double) NUMBER_OF_ITERATIONS;
		if (RandomGenerator.getRandomDouble() < probability) {
			return RandomGenerator.getRandomInteger(0, numberOfActions - 1);
		}

		return getMaxRewardAction(values);
	}
}