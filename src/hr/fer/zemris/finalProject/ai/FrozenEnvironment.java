package hr.fer.zemris.finalProject.ai;

import hr.fer.zemris.finalProject.game.KeyManager;
import hr.fer.zemris.finalProject.game.Player;
import hr.fer.zemris.finalProject.game.frozenLake.FrozenLakeSimulator;

public class FrozenEnvironment<T> implements Environment<T> {

	private static final int NUMBER_OF_ACTIONS = 4;

	private double windProbability;

	private FrozenLakeSimulator simulator;

	private Player player;

	private KeyManager keyManager;

	private StateCreator<T> stateCreator;

	public FrozenEnvironment(StateCreator<T> stateCreator, double windProbability) {
		this.stateCreator = stateCreator;
		this.windProbability = windProbability;

		keyManager = new KeyManager();
	}

	@Override
	public int getNumberOfActions() {
		return NUMBER_OF_ACTIONS;
	}

	@Override
	public int getNumberOfInputs() {
		int[][] map = FrozenLakeSimulator.getMap();
		return map[0].length * map.length;
	}

	@Override
	public T step(int action) {
		keyManager.setKey(action);
		simulator.gameStep();

		player = simulator.getGameState().getPlayer();

		return stateCreator.getState(simulator.getGameState());
	}

	@Override
	public T initialize() {
		simulator = new FrozenLakeSimulator(keyManager, 0, windProbability);
		player = simulator.getGameState().getPlayer();

		return stateCreator.getState(simulator.getGameState());
	}

	@Override
	public double getCurrentStateReward() {
		double reward = -3;

		if (player.isDead()) {
			reward = -100;
		} else if (player.isWinner()) {
			reward = 200;
		}

		return reward;
	}

	@Override
	public boolean gameFinished() {
		return player.isDead() || player.isWinner();
	}

	@Override
	public int getFinalReward() {
		return player.isWinner() ? 200 : 0;
	}
}