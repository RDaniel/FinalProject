package hr.fer.zemris.finalProject.ai.neuralNetwork.layer;


import hr.fer.zemris.finalProject.ai.neuralNetwork.functions.ActivationFunction;
import hr.fer.zemris.finalProject.ai.neuralNetwork.neuron.Connection;
import hr.fer.zemris.finalProject.ai.neuralNetwork.neuron.Neuron;
import hr.fer.zemris.finalProject.game.flappy.util.RandomGenerator;

public class LayerUtils {

	public static final double STD_DEV = 0.01;

	public static Layer createInputLayer(int numberOfNeutrons, ActivationFunction activationFunction) {
		final int layerIndex = 0;
		Neuron[] inputNeurons = new Neuron[numberOfNeutrons];

		int counter = 0;
		while (counter < numberOfNeutrons) {
			inputNeurons[counter] = new Neuron(
					layerIndex + "" + counter++,
					0);
		}

		return new Layer(inputNeurons, activationFunction);
	}

	public static Layer createHiddenLayer(int numberOfNeutrons, int layerIndex, ActivationFunction activationFunction) {
		Neuron[] hiddenNeurons = new Neuron[numberOfNeutrons];

		int counter = 0;
		while (counter < numberOfNeutrons) {
			hiddenNeurons[counter] = new Neuron(
					layerIndex + "" + counter++,
					RandomGenerator.getGaussian() * STD_DEV);
		}

		return new Layer(hiddenNeurons, activationFunction);
	}

	public static Layer createOutputLayer(int numberOfNeutrons, int layerIndex, ActivationFunction activationFunction) {
		Neuron[] outputNeurons = new Neuron[numberOfNeutrons];

		int counter = 0;
		while (counter < numberOfNeutrons) {
			outputNeurons[counter] = new Neuron(
					layerIndex + "" + counter++,
					RandomGenerator.getGaussian() * STD_DEV);
		}

		return new Layer(outputNeurons, activationFunction);
	}

	public static void createConnections(Layer source, Layer destination) {
		Neuron[] sourceNeurons = source.getNeurons();
		Neuron[] destinationNeurons = destination.getNeurons();

		for (Neuron sourceNeuron : sourceNeurons) {
			for (Neuron destinationNeuron : destinationNeurons) {
				Connection connection = new Connection(sourceNeuron, destinationNeuron, RandomGenerator.getGaussian() * STD_DEV);
				sourceNeuron.addOutputConnection(connection);
				destinationNeuron.addInputConnection(connection);
			}
		}
	}
}