package hr.fer.zemris.finalProject.ai.neuralNetwork.layer;


import hr.fer.zemris.finalProject.ai.neuralNetwork.functions.ActivationFunction;
import hr.fer.zemris.finalProject.ai.neuralNetwork.neuron.Neuron;

public class Layer {

	private Neuron[] neurons;

	private ActivationFunction activationFunction;

	public Layer(Neuron[] neurons, ActivationFunction activationFunction) {
		this.neurons = neurons;
		this.activationFunction = activationFunction;
	}

	public Neuron[] getNeurons() {
		return neurons;
	}

	public ActivationFunction getActivationFunction() {
		return activationFunction;
	}

	public void calculateNeuronOutputs() {
		for (Neuron neuron : neurons) {
			neuron.calculateOutput(activationFunction);
		}
	}

	public double[] getNeuronOutputs() {
		double[] neuronOutputs = new double[neurons.length];

		for (int i = 0; i < neurons.length; i++) {
			neuronOutputs[i] = neurons[i].getOutput();
		}

		return neuronOutputs;
	}
}