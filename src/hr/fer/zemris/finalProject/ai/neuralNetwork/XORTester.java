package hr.fer.zemris.finalProject.ai.neuralNetwork;

import hr.fer.zemris.finalProject.ai.neuralNetwork.functions.ActivationFunction;
import hr.fer.zemris.finalProject.ai.neuralNetwork.functions.IdentityFunction;
import hr.fer.zemris.finalProject.ai.neuralNetwork.functions.ReLUFunction;

public class XORTester {

	private static final double ACCEPTABLE_ERROR = 0.0001;

	private static final double inputs[][] = {{1, 1}, {1, 0}, {0, 1}, {0, 0}};

	private static final double expectedOutputs[] = {0, 1, 1, 0};

	public static void main(String[] args) {
		int[] layout = new int[]{2, 20, 1};
		ActivationFunction[] functions = new ActivationFunction[]{
				new IdentityFunction(),
				new ReLUFunction(),
				new IdentityFunction()
		};

		NeuralArchitecture architecture = new NeuralArchitecture(layout, functions);
		NeuralNetwork network = new FeedForwardNeuralNetwork(architecture);

		for (int i = 0; i < 10000; i++) {
			double error = 0;
			for (int j = 0; j < 4; j++) {
				double[] output = network.forwardPass(inputs[j]);
				error += Math.pow(output[0] - expectedOutputs[j], 2) * 0.5;
				network.updateValues(new double[]{(output[0] - expectedOutputs[j])});
			}

			System.out.println(error);

			if (error < ACCEPTABLE_ERROR) {
				System.out.println("We learned it at " + i + ". iteration !");
				break;
			}
		}
	}
}