package hr.fer.zemris.finalProject.ai.neuralNetwork;


import hr.fer.zemris.finalProject.ai.neuralNetwork.functions.ActivationFunction;
import hr.fer.zemris.finalProject.ai.neuralNetwork.layer.Layer;
import hr.fer.zemris.finalProject.ai.neuralNetwork.layer.LayerUtils;
import hr.fer.zemris.finalProject.ai.neuralNetwork.neuron.Connection;
import hr.fer.zemris.finalProject.ai.neuralNetwork.neuron.Neuron;

import java.util.List;

public class FeedForwardNeuralNetwork implements NeuralNetwork {

	private static final double MOMENTUM = 0.6;
	private static final double DEFAULT_LEARNING_RATE = 1E-3;

	private int[] architecture;

	private double learningRate;

	private NeuralArchitecture neuralArchitecture;

	private int numberOfWeights;

	private Layer inputLayer;

	private Layer[] layers;

	private Layer outputLayer;

	public FeedForwardNeuralNetwork(NeuralArchitecture neuralArchitecture) {
		this(neuralArchitecture, DEFAULT_LEARNING_RATE);
	}

	public FeedForwardNeuralNetwork(NeuralArchitecture neuralArchitecture, double learningRate) {
		this.architecture = neuralArchitecture.getLayout();
		this.neuralArchitecture = neuralArchitecture;
		this.learningRate = learningRate;

		layers = new Layer[architecture.length];

		numberOfWeights = calculateNumberOfWeights(architecture);
		createNetworkLayers(neuralArchitecture.getActivationFunctions());
	}


	public static int calculateNumberOfWeights(int[] architecture) {
		int numberOfWeights = 0;
		for (int i = 0; i < architecture.length - 1; i++) {
			numberOfWeights += architecture[i] * architecture[i + 1] + architecture[i + 1];
		}

		return numberOfWeights;
	}

	@Override
	public int getNumberOfNetworkWeights() {
		return numberOfWeights;
	}

	@Override
	public NeuralArchitecture getArchitecture() {
		return neuralArchitecture;
	}

	/**
	 * Returns double array that contains neural network weights.
	 * Every layer, except first, puts for every neuron his input connections weights and the bias.
	 * The order of weights corresponds to the order in layer array.
	 *
	 * @return double array that contains neural network weights
	 */
	@Override
	public double[] getNetworkWeights() {
		double[] networkWeights = new double[numberOfWeights];

		int currentIndex = 0;
		for (int i = 1; i < layers.length; i++) {
			Neuron[] neurons = layers[i].getNeurons();
			for (Neuron neuron : neurons) {
				for (Connection connection : neuron.getInputConnections()) {
					networkWeights[currentIndex++] = connection.getWeight();
				}

				networkWeights[currentIndex++] = neuron.getBias();
			}
		}

		return networkWeights;
	}

	@Override
	public void setNetworkWeights(double[] networkWeights) {
		if (numberOfWeights != networkWeights.length) {
			throw new IllegalArgumentException("Wrong number of weights.");
		}

		int currentIndex = 0;
		for (int i = 1; i < layers.length; i++) {
			Neuron[] neurons = layers[i].getNeurons();
			for (Neuron neuron : neurons) {
				for (Connection connection : neuron.getInputConnections()) {
					connection.setWeight(networkWeights[currentIndex++]);
				}

				neuron.setBias(networkWeights[currentIndex++]);
			}
		}
	}

	@Override
	public double getLearningRate() {
		return learningRate;
	}

	@Override
	public double[] forwardPass(double[] inputs) {
		if (inputs.length != architecture[0]) {
			throw new IllegalArgumentException("Wrong number of inputs!");
		}

		setNeuralNetworkInputs(inputs);
		for (Layer layer : layers) {
			layer.calculateNeuronOutputs();
		}

		return outputLayer.getNeuronOutputs();
	}

	@Override
	public void updateValues(double[] lossGradients) {
		if (lossGradients.length != architecture[architecture.length - 1]) {
			throw new IllegalArgumentException("Wrong gradients number.");
		}

		backwardPass(lossGradients);
		stochasticGradientDescent();
	}

	private void stochasticGradientDescent() {
		for (int i = 1; i < layers.length; i++) {
			Layer layer = layers[i];
			Neuron[] neurons = layer.getNeurons();
			for (Neuron neuron : neurons) {
				List<Connection> inputConnections = neuron.getInputConnections();
				for (Connection inputConnection : inputConnections) {
					inputConnection.setWeight(inputConnection.getWeight() - learningRate * inputConnection.getWeightError());
				}

				neuron.setBias(neuron.getBias() - learningRate * neuron.getBiasError());
			}
		}
	}

	private void backwardPass(double[] lossGradients) {
		// calculate output neurons error
		Neuron[] outputNeurons = outputLayer.getNeurons();
		ActivationFunction activation = outputLayer.getActivationFunction();
		for (int i = 0; i < outputNeurons.length; i++) {
			Neuron neuron = outputNeurons[i];
			neuron.setNeuronError(lossGradients[i] * activation.calculateDerivative(neuron.getNet()));
		}

		//propagate error backward
		for (int i = layers.length - 2; i >= 1; i--) {
			Neuron[] neurons = layers[i].getNeurons();
			activation = layers[i].getActivationFunction();
			for (Neuron neuron : neurons) {

				double neuronError = 0;
				List<Connection> outputConnections = neuron.getOutputConnections();
				for (Connection outputConnection : outputConnections) {
					neuronError += outputConnection.getWeight() * outputConnection.getDestination().getNeuronError();
				}

				neuronError *= activation.calculateDerivative(neuron.getNet());
				neuron.setNeuronError(neuronError);
			}
		}

		//calculate weights error
		for (int i = 1; i < layers.length; i++) {
			Neuron[] neurons = layers[i].getNeurons();
			for (Neuron neuron : neurons) {
				neuron.setBiasError(MOMENTUM * neuron.getBiasError() + (1 - MOMENTUM) * neuron.getNeuronError());

				List<Connection> inputConnections = neuron.getInputConnections();
				for (Connection input : inputConnections) {
					input.setWeightError(MOMENTUM * input.getWeightError() +
							(1 - MOMENTUM) * input.getSource().getOutput() * neuron.getNeuronError());
				}
			}
		}
	}

	private void setNeuralNetworkInputs(double[] inputs) {
		Neuron[] inputNeurons = inputLayer.getNeurons();

		for (int i = 0; i < inputNeurons.length; i++) {
			inputNeurons[i].setDirectInput(inputs[i]);
		}
	}

	private void createNetworkLayers(ActivationFunction[] activationFunctions) {
		for (int i = 0; i < architecture.length; i++) {
			if (i == 0) {
				inputLayer = LayerUtils.createInputLayer(architecture[i], activationFunctions[0]);
				layers[i] = inputLayer;
			} else if (i == architecture.length - 1) {
				outputLayer = LayerUtils.createOutputLayer(architecture[i], i, activationFunctions[2]);
				layers[i] = outputLayer;
			} else {
				layers[i] = LayerUtils.createHiddenLayer(architecture[i], i, activationFunctions[1]);
			}
		}

		createNetworkConnections();
	}

	private void createNetworkConnections() {
		for (int i = 0, j = layers.length - 1; i < j; i++) {
			LayerUtils.createConnections(layers[i], layers[i + 1]);
		}
	}
}