package hr.fer.zemris.finalProject.ai.neuralNetwork.neuron;

import hr.fer.zemris.finalProject.ai.neuralNetwork.functions.ActivationFunction;

import java.util.ArrayList;
import java.util.List;

public class Neuron {

	private String index;

	private double net;

	private double bias;

	private double output;

	private double biasError;

	private double neuronError;

	private double directInput;

	private List<Connection> inputConnections;

	private List<Connection> outputConnections;

	public Neuron(String index, double bias) {
		this.index = index;
		this.bias = bias;

		inputConnections = new ArrayList<>();
		outputConnections = new ArrayList<>();
	}

	public double getOutput() {
		return output;
	}

	public double getBias() {
		return bias;
	}

	public void setBias(double bias) {
		this.bias = bias;
	}

	public double getNet() {
		return net;
	}

	public double getNeuronError() {
		return neuronError;
	}

	public void setNeuronError(double neuronError) {
		this.neuronError = neuronError;
	}

	public double getBiasError() {
		return biasError;
	}

	public void setBiasError(double biasError) {
		this.biasError = biasError;
	}

	public void setDirectInput(double directInput) {
		this.directInput = directInput;
	}

	public List<Connection> getInputConnections() {
		return inputConnections;
	}

	public List<Connection> getOutputConnections() {
		return outputConnections;
	}

	public void addInputConnection(Connection inputConnection) {
		inputConnections.add(inputConnection);
	}

	public void addOutputConnection(Connection outputConnection) {
		outputConnections.add(outputConnection);
	}

	public void calculateOutput(ActivationFunction activationFunction) {
		net = 0;

		for (Connection connection : inputConnections) {
			net += connection.getWeight() * connection.getSource().getOutput();
		}
		net += bias + directInput;

		output = activationFunction.calculateOutput(net);
	}
}