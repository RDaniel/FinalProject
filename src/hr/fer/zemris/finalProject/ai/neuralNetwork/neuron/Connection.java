package hr.fer.zemris.finalProject.ai.neuralNetwork.neuron;

public class Connection {

	private final Neuron source;

	private final Neuron destination;

	private double weight;

	private double weightError;

	public Connection(Neuron source, Neuron destination, double weight) {
		this.source = source;
		this.destination = destination;
		this.weight = weight;
	}

	public double getWeightError() {
		return weightError;
	}

	public void setWeightError(double weightError) {
		this.weightError = weightError;
	}

	public Neuron getSource() {
		return source;
	}

	public Neuron getDestination() {
		return destination;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}
}