package hr.fer.zemris.finalProject.ai.neuralNetwork.functions;

public class IdentityFunction implements ActivationFunction {

	private static final String NAME = "PROPAGATE";

	@Override
	public double calculateOutput(double net) {
		return net;
	}

	@Override
	public double calculateDerivative(double net) {
		return 1;
	}

	@Override
	public String getName() {
		return NAME;
	}
}