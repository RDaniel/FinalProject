package hr.fer.zemris.finalProject.ai.neuralNetwork.functions;

public interface ActivationFunction {

	double calculateOutput(double net);

	double calculateDerivative(double net);

	String getName();
}