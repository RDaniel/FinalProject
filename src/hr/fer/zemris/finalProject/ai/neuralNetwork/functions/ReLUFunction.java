package hr.fer.zemris.finalProject.ai.neuralNetwork.functions;

public class ReLUFunction implements ActivationFunction {

	private static final String NAME = "ReLU";

	@Override
	public double calculateOutput(double net) {
		return net >= 0 ? net : 0;
	}

	@Override
	public double calculateDerivative(double net) {
		return net >= 0 ? 1 : 0;
	}

	@Override
	public String getName() {
		return NAME;
	}
}
