package hr.fer.zemris.finalProject.ai.neuralNetwork.functions;

public class SquaredSumLossFunction implements LossFunction {

	@Override
	public double calculateLoss(double[] current, double[] target) {
		if (current.length != target.length) {
			throw new IllegalArgumentException("Different array lengths.");
		}

		double totalLoss = 0;
		for (int i = 0; i < current.length; i++) {
			totalLoss += (target[i] - current[i]) * (target[i] - current[i]);
		}

		return totalLoss * 0.5;
	}

	@Override
	public double[] calculateGradients(double[] current, double[] target) {
		double[] gradients = new double[current.length];

		for (int i = 0; i < current.length; i++) {
			gradients[i] = current[i] - target[i];
		}

		return gradients;
	}
}