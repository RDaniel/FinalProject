package hr.fer.zemris.finalProject.ai.neuralNetwork.functions;

public class SigmoidalFunction implements ActivationFunction {

	private final static String NAME = "SIGMOIDAL";

	@Override
	public double calculateOutput(double net) {
		return 1 / (1 + Math.exp(-net));
	}

	@Override
	public double calculateDerivative(double net) {
		return calculateOutput(net) * (1 - calculateOutput(net));
	}

	@Override
	public String getName() {
		return NAME;
	}
}