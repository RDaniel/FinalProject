package hr.fer.zemris.finalProject.ai.neuralNetwork.functions;

public class TangensHyperbolic implements ActivationFunction {

	private final static String NAME = "TANH";

	@Override
	public double calculateOutput(double net) {
		double e2x = Math.pow(Math.E, 2 * net);
		return (e2x - 1) / (e2x + 1);
	}

	@Override
	public double calculateDerivative(double net) {
		double output = calculateOutput(net);
		return 1 - output * output;
	}

	@Override
	public String getName() {
		return NAME;
	}
}
