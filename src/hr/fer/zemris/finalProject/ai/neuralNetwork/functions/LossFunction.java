package hr.fer.zemris.finalProject.ai.neuralNetwork.functions;

public interface LossFunction {

	double calculateLoss(double[] current, double target[]);

	double[] calculateGradients(double[] current, double[] target);

}