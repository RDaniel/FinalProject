package hr.fer.zemris.finalProject.ai.neuralNetwork;

public interface NeuralNetwork {

	NeuralArchitecture getArchitecture();

	int getNumberOfNetworkWeights();

	double[] getNetworkWeights();

	void setNetworkWeights(double[] networkWeights);

	double getLearningRate();

	double[] forwardPass(double[] inputs);

	void updateValues(double[] lossGradients);
}