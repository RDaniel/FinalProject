package hr.fer.zemris.finalProject.ai.neuralNetwork;


import hr.fer.zemris.finalProject.ai.neuralNetwork.functions.ActivationFunction;

/**
 * The architecture of a neural network.
 * An instance of this class encapsulates the layout of
 * the neural network and the {@link ActivationFunction}s
 * to be used by each of the network's layers for calculating
 * the output.
 */
public class NeuralArchitecture {

	/**
	 * The layout of the neural network.
	 */
	private int[] layout;

	/**
	 * The activation activationFunctions.
	 */
	private ActivationFunction[] activationFunctions;

	/**
	 * Creates a new {@link NeuralArchitecture} object.
	 *
	 * @param layout              the layout of the network (the size of the array is
	 *                            equal to the number of layers; the value of each element
	 *                            is equal to the number of neurons per layer)
	 * @param activationFunctions the activation activationFunctions (for each of the layers)
	 */
	public NeuralArchitecture(int[] layout, ActivationFunction[] activationFunctions) {
		this.layout = layout;
		this.activationFunctions = activationFunctions;
	}

	/**
	 * Gets the layout.
	 *
	 * @return the layout
	 */
	public int[] getLayout() {
		return layout;
	}

	/**
	 * Gets the activation activationFunctions.
	 *
	 * @return the activation activationFunctions
	 */
	public ActivationFunction[] getActivationFunctions() {
		return activationFunctions;
	}
}
