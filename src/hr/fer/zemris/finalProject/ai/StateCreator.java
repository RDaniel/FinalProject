package hr.fer.zemris.finalProject.ai;

import hr.fer.zemris.finalProject.game.GameState;

public interface StateCreator<T> {

	T getState(GameState gameState);

}