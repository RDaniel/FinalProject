package hr.fer.zemris.finalProject.ai;

import hr.fer.zemris.finalProject.game.flappy.util.RandomGenerator;

import java.util.ArrayList;
import java.util.List;

public class QTableLearner {

	private static final int NUMBER_OF_GAME_ITERATIONS = 50;

	private int numberOfIterations;
	private double epsilonProbability;
	private double learningRate;
	private double discountFactor;
	private double[][] Q;
	private Environment<Integer> environment;

	private List<SolutionListener> solutionListeners;

	public QTableLearner(Environment<Integer> environment, int numberOfIterations, double epsilonProbability, double learningRate, double discountFactor) {
		this.environment = environment;
		this.numberOfIterations = numberOfIterations;
		this.epsilonProbability = epsilonProbability;
		this.discountFactor = discountFactor;
		this.learningRate = learningRate;

		Q = new double[environment.getNumberOfInputs()][environment.getNumberOfActions()];
		solutionListeners = new ArrayList<>();
	}

	public double[][] getQ() {
		return Q;
	}

	public void learn() {
		for (int i = 0; i < numberOfIterations; i++) {
			int state = environment.initialize();
			double totalReward = 0;
			for (int j = 0; j < NUMBER_OF_GAME_ITERATIONS; j++) {
				int action = getNextAction(state);
				int nextState = environment.step(action);
				double reward = environment.getCurrentStateReward();

				totalReward += reward;
				int maxIndex = getMaxAction(Q[nextState]);
				Q[state][action] += learningRate * (reward + discountFactor * Q[nextState][maxIndex] - Q[state][action]);

				if (environment.gameFinished()) {
					break;
				}

				state = nextState;
			}
			for (SolutionListener sol : solutionListeners) {
				sol.solutionGenerated(i, totalReward);
			}
		}
	}

	public void addSolutionListener(SolutionListener solutionListener) {
		solutionListeners.add(solutionListener);
	}

	public void removeSolutionListner(SolutionListener solutionListener) {
		solutionListeners.add(solutionListener);
	}

	private int getMaxAction(double[] values) {
		double maximum = -Double.MAX_VALUE;
		int index = -1;
		for (int i = 0; i < values.length; i++) {
			if (values[i] > maximum) {
				maximum = values[i];
				index = i;
			}
		}

		return index;
	}

	private int getNextAction(int state) {
		if (RandomGenerator.getRandomDouble() < epsilonProbability) {
			return RandomGenerator.getRandomInteger(0, environment.getNumberOfActions() - 1);
		}

		return getMaxAction(Q[state]);
	}
}