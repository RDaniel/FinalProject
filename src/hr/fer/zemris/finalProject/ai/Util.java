package hr.fer.zemris.finalProject.ai;


import hr.fer.zemris.finalProject.ai.neuralNetwork.FeedForwardNeuralNetwork;
import hr.fer.zemris.finalProject.ai.neuralNetwork.NeuralArchitecture;
import hr.fer.zemris.finalProject.ai.neuralNetwork.NeuralNetwork;
import hr.fer.zemris.finalProject.ai.neuralNetwork.functions.ActivationFunction;
import hr.fer.zemris.finalProject.ai.neuralNetwork.functions.IdentityFunction;
import hr.fer.zemris.finalProject.ai.neuralNetwork.functions.ReLUFunction;
import hr.fer.zemris.finalProject.ai.neuralNetwork.functions.SigmoidalFunction;
import hr.fer.zemris.finalProject.gui.Constants;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Util {

	private static Map<String, ActivationFunction> activationMap;

	static {
		activationMap = new HashMap<>();
		activationMap.put("ReLU", new ReLUFunction());
		activationMap.put("PROPAGATE", new IdentityFunction());
		activationMap.put("SIGMOIDAL", new SigmoidalFunction());
	}

	public static InputStream getDefaultPlayer(String name) {
		return Util.class.getResourceAsStream(File.separator + name);
	}

	public static InputStream getCustomPlayer(String name) {
		try {
			return new FileInputStream(new File(Constants.CUSTOM_ROOT_DIR + File.separator + name));
		} catch (FileNotFoundException e) {
			System.err.println(e.toString());
		}

		return null;
	}

	public static void saveTable(double[][] table, Path path) {
		deletePrevious(path);

		try (BufferedWriter writer = Files.newBufferedWriter(path, StandardOpenOption.CREATE)) {

			writer.write(table.length + "x" + table[0].length + System.lineSeparator());
			for (double[] stateActions : table) {
				writer.write(Arrays.toString(stateActions) + System.lineSeparator());
			}

			writer.flush();
		} catch (IOException ex) {
			System.err.println("Cannot save table.");
		}
	}

	public static double[][] loadTable(InputStream inputStream) {
		List<String> lines;
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
			lines = reader.lines().collect(Collectors.toList());
		} catch (IOException ex) {
			throw new IllegalArgumentException("Invalid path given, player does not exit!");
		}

		String[] tableDefinition = lines.get(0).split("x");
		double[][] table = new double[Integer.parseInt(tableDefinition[0])][Integer.parseInt(tableDefinition[1])];

		for (int i = 0; i < table.length; i++) {
			String[] state = lines.get(i + 1).replace("[", "").
					replace("]", "").
					trim().split(", ");

			for (int j = 0; j < state.length; j++) {
				table[i][j] = Double.parseDouble(state[j]);
			}
		}

		return table;
	}

	public static void saveNetwork(Path path, NeuralNetwork neuralNetwork) {
		deletePrevious(path);

		try (BufferedWriter writer = Files.newBufferedWriter(path, StandardOpenOption.CREATE)) {
			NeuralArchitecture architecture = neuralNetwork.getArchitecture();
			int[] layout = architecture.getLayout();
			writer.write(removeBrackets(Arrays.toString(layout)) + System.lineSeparator());

			ActivationFunction[] functions = architecture.getActivationFunctions();
			for (ActivationFunction function : functions) {
				writer.write(function.getName() + System.lineSeparator());
			}

			double[] networkWeights = neuralNetwork.getNetworkWeights();
			writer.write(removeBrackets(Arrays.toString(networkWeights)));

			writer.flush();
		} catch (IOException ex) {
			System.err.println("Cannot save network.");
		}
	}

	public static NeuralNetwork loadNetwork(InputStream inputStream) {
		NeuralNetwork neuralNetwork;
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
			String line = reader.readLine();
			int[] architecture = Arrays.stream(line.split(", ")).mapToInt(Integer::parseInt).toArray();

			ActivationFunction[] functions = new ActivationFunction[3];
			functions[0] = activationMap.get(reader.readLine());
			functions[1] = activationMap.get(reader.readLine());
			functions[2] = activationMap.get(reader.readLine());

			line = reader.readLine();
			double[] weights = Arrays.stream(line.split(", ")).mapToDouble(Double::parseDouble).toArray();

			NeuralArchitecture neuralArchitecture = new NeuralArchitecture(architecture, functions);
			neuralNetwork = new FeedForwardNeuralNetwork(neuralArchitecture);
			neuralNetwork.setNetworkWeights(weights);
		} catch (IOException ex) {
			throw new IllegalArgumentException("Invalid path given, player does not exit!");
		}

		return neuralNetwork;
	}

	private static String removeBrackets(String str) {
		return str.replace("[", "").replace("]", "");
	}

	private static void deletePrevious(Path path) {
		try {
			Files.deleteIfExists(path);
		} catch (IOException ignored) {
		}
	}
}