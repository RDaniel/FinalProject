package hr.fer.zemris.finalProject.ai;

public interface Environment<T> {

	T initialize();

	double getCurrentStateReward();

	T step(int action);

	int getNumberOfInputs();

	int getNumberOfActions();

	boolean gameFinished();

	int getFinalReward();

}