package hr.fer.zemris.finalProject.ai;

public interface SolutionListener {

	void solutionGenerated(long iteration, double solutionFitness);

}
