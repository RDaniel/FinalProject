package hr.fer.zemris.finalProject.ai;

import hr.fer.zemris.finalProject.ai.neuralNetwork.FeedForwardNeuralNetwork;
import hr.fer.zemris.finalProject.ai.neuralNetwork.NeuralArchitecture;
import hr.fer.zemris.finalProject.ai.neuralNetwork.NeuralNetwork;
import hr.fer.zemris.finalProject.ai.neuralNetwork.functions.*;
import hr.fer.zemris.finalProject.game.flappy.util.RandomGenerator;

import java.util.*;

public class QNeuralNetworkLearner {

	private static final int REPLAY_MEMORY_BUFFER = 256;
	private static final int BATCH_SIZE = 32;
	private static final int TARGET_UPDATE = 100;

	private final int replayMemorySize;
	private final int batchSize;
	private final int targetUpdate;

	private long numberOfGameIterations;
	private long numberOfIterations;
	private double epsilonProbability;
	private double discountFactor;
	private int numberOfActions;
	private double[] maxWeights;
	private List<SolutionListener> solutionListeners;

	private NeuralNetwork mainNet;
	private NeuralNetwork targetNetwork;
	private Queue<Experience> experience = new LinkedList<>();

	private Environment<double[]> environment;

	private LossFunction lossFunction;

	public QNeuralNetworkLearner(Environment<double[]> environment, boolean experienceReplay, boolean targetNetwork, long numberOfIterations, long numberOfGameIterations, double epsilonProbability, double discountFactor, double learningRate) {
		this.environment = environment;
		this.numberOfIterations = numberOfIterations;
		this.epsilonProbability = epsilonProbability;
		this.discountFactor = discountFactor;
		this.numberOfGameIterations = numberOfGameIterations;
		numberOfActions = environment.getNumberOfActions();
		solutionListeners = new ArrayList<>();

		batchSize = experienceReplay ? BATCH_SIZE : 1;
		replayMemorySize = experienceReplay ? REPLAY_MEMORY_BUFFER : 1;
		targetUpdate = targetNetwork ? TARGET_UPDATE : 1;

		createNetworks(environment, learningRate);

		lossFunction = new SquaredSumLossFunction();
	}

	private void createNetworks(Environment<double[]> environment, double learningRate) {
		int[] layout = new int[]{environment.getNumberOfInputs(), 20, numberOfActions};
		ActivationFunction[] functions = new ActivationFunction[]{
				new IdentityFunction(),
				new ReLUFunction(),
				new IdentityFunction()
		};
		NeuralArchitecture architecture = new NeuralArchitecture(layout, functions);
		mainNet = new FeedForwardNeuralNetwork(architecture, learningRate);
		targetNetwork = new FeedForwardNeuralNetwork(architecture, learningRate);
		targetNetwork.setNetworkWeights(mainNet.getNetworkWeights());
	}

	public NeuralNetwork learn() {
		double maxRew = -Double.MAX_VALUE;
		for (long i = 0; i < numberOfIterations; i++) {
			double[] state = environment.initialize();
			int j;
			for (j = 0; j < numberOfGameIterations; j++) {
				double[] qValues = mainNet.forwardPass(state);
				int action = epsilonGreedActionSelection(qValues, i);
				double[] nextState = environment.step(action);
				double reward = environment.getCurrentStateReward();
				Experience current = new Experience(state, action, nextState, reward);

				List<Experience> learningBatch = pickLearningBatch();
				learningBatch.add(current);
				experience.add(current);

				double[] batchGradient = calculateBatchGradient(learningBatch);
				mainNet.updateValues(batchGradient);
				if (experience.size() >= replayMemorySize) {
					experience.poll();
				}

				if (environment.gameFinished()) {
					break;
				}

				state = nextState;
			}

			for (SolutionListener listener : solutionListeners) {
				listener.solutionGenerated((int) i, environment.getFinalReward());
			}

			if (environment.getFinalReward() > maxRew) {
				System.out.println(environment.getFinalReward());
				maxRew = environment.getFinalReward();
				maxWeights = mainNet.getNetworkWeights();
			}

			if (i % targetUpdate == 0) {
				targetNetwork.setNetworkWeights(mainNet.getNetworkWeights());
			}
		}

		mainNet.setNetworkWeights(maxWeights);
		return mainNet;
	}

	public void addSolutionListener(SolutionListener solutionListener) {
		solutionListeners.add(solutionListener);
	}

	public void removeSolutionListner(SolutionListener solutionListener) {
		solutionListeners.add(solutionListener);
	}

	private double[] calculateBatchGradient(List<Experience> learningBatch) {
		double[] gradients = new double[numberOfActions];
		for (Experience batch : learningBatch) {
			double[] oldQVal = mainNet.forwardPass(batch.state);
			double[] targetValues = targetNetwork.forwardPass(batch.nextState);
			int rewardAction = getMaxRewardAction(targetValues);
			double[] target = Arrays.copyOf(oldQVal, oldQVal.length);
			target[batch.action] = batch.reward + discountFactor * targetValues[rewardAction];
			double[] sampleGradients = lossFunction.calculateGradients(oldQVal, target);

			for (int i = 0; i < gradients.length; i++) {
				gradients[i] += sampleGradients[i];
			}
		}

		for (int i = 0, j = learningBatch.size(); i < gradients.length; i++) {
			gradients[i] /= (double) j;
		}

		return gradients;
	}

	private int getMaxRewardAction(double[] values) {
		double maximum = -Double.MAX_VALUE;
		int index = -1;
		for (int i = 0; i < values.length; i++) {
			if (values[i] > maximum) {
				maximum = values[i];
				index = i;
			}
		}

		return index;
	}

	private int epsilonGreedActionSelection(double[] values, long currentIteration) {
		double probability = epsilonProbability * (numberOfIterations - currentIteration) / (double) numberOfIterations;
		if (RandomGenerator.getRandomDouble() < probability) {
			return RandomGenerator.getRandomInteger(0, numberOfActions - 1);
		}

		return getMaxRewardAction(values);
	}

	private List<Experience> pickLearningBatch() {
		List<Experience> copy = new LinkedList<>(experience);
		Collections.shuffle(copy);
		return copy.subList(0, Math.min(experience.size(), batchSize));
	}

	private static class Experience {
		double[] state;
		int action;
		double[] nextState;
		double reward;

		Experience(double[] state, int action, double[] nextState, double reward) {
			this.state = state;
			this.action = action;
			this.nextState = nextState;
			this.reward = reward;
		}
	}
}