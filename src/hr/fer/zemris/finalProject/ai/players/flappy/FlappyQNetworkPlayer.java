package hr.fer.zemris.finalProject.ai.players.flappy;

import hr.fer.zemris.finalProject.ai.players.QNetworkPlayer;

import java.io.InputStream;

public class FlappyQNetworkPlayer extends QNetworkPlayer implements FlappyGamePlayer {

	public FlappyQNetworkPlayer(InputStream networkStream) {
		super(networkStream);
	}

	@Override
	public double[] getQValues(double[] position) {
		return network.forwardPass(position);
	}
}