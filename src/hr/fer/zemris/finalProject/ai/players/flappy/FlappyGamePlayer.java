package hr.fer.zemris.finalProject.ai.players.flappy;

import hr.fer.zemris.finalProject.ai.players.GamePlayer;

public interface FlappyGamePlayer extends GamePlayer {

	double[] getQValues(double[] position);

}