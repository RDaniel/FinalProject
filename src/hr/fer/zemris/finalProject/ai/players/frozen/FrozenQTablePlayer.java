package hr.fer.zemris.finalProject.ai.players.frozen;

import hr.fer.zemris.finalProject.ai.players.QTablePlayer;

import java.io.InputStream;

public class FrozenQTablePlayer extends QTablePlayer implements FrozenGamePlayer {

	public FrozenQTablePlayer(InputStream playerStream) {
		super(playerStream);
	}

	@Override
	public double[] getQValues() {
		double[] qValues = new double[Q.length * Q[0].length];
		int counter = 0;
		for (double[] values : Q) {
			for (double value : values) {
				qValues[counter++] = value;
			}
		}

		return qValues;
	}
}
