package hr.fer.zemris.finalProject.ai.players.frozen;

import hr.fer.zemris.finalProject.ai.players.GamePlayer;

public interface FrozenGamePlayer extends GamePlayer {

	double[] getQValues();

}
