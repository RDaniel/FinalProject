package hr.fer.zemris.finalProject.ai.players.frozen;

import hr.fer.zemris.finalProject.ai.players.QNetworkPlayer;
import hr.fer.zemris.finalProject.game.frozenLake.FrozenLakeSimulator;

import java.io.InputStream;
import java.util.Arrays;

public class FrozenQNetworkPlayer extends QNetworkPlayer implements FrozenGamePlayer {

	public FrozenQNetworkPlayer(InputStream networkPath) {
		super(networkPath);
	}

	@Override
	public double[] getQValues() {
		int[][] map = FrozenLakeSimulator.getMap();
		int numberOfStates = map.length * map[0].length;
		double[] qValues = new double[numberOfStates * 4];

		double[] position = new double[numberOfStates];
		for (int i = 0; i < numberOfStates; i++) {
			position[i] = 1;

			double[] stateQValues = network.forwardPass(position);
			qValues[4 * i] = stateQValues[0];
			qValues[4 * i + 1] = stateQValues[1];
			qValues[4 * i + 2] = stateQValues[2];
			qValues[4 * i + 3] = stateQValues[3];

			Arrays.fill(position, 0);
		}

		return qValues;
	}
}