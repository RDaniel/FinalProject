package hr.fer.zemris.finalProject.ai.players;

import hr.fer.zemris.finalProject.ai.Util;
import hr.fer.zemris.finalProject.ai.neuralNetwork.NeuralNetwork;
import hr.fer.zemris.finalProject.game.GameState;
import hr.fer.zemris.finalProject.game.KeyManager;

import java.io.InputStream;

public abstract class QNetworkPlayer implements GamePlayer {

	protected NeuralNetwork network;

	private KeyManager keyManager;

	public QNetworkPlayer(InputStream networkStream) {
		network = Util.loadNetwork(networkStream);
	}

	@Override
	public void gameStateChanged(GameState gameState) {
		if (keyManager == null) {
			throw new IllegalStateException("Key manager has to be initialized.");
		}

		double[] inputs = gameState.getAIInputs();
		double[] qValues = network.forwardPass(inputs);

		keyManager.setKey(getMaxRewardAction(qValues));
	}

	private int getMaxRewardAction(double[] values) {
		double maximum = -Double.MAX_VALUE;
		int index = -1;
		for (int i = 0; i < values.length; i++) {
			if (values[i] > maximum) {
				maximum = values[i];
				index = i;
			}
		}

		return index;
	}

	@Override
	public void setKeyManager(KeyManager keyManager) {
		this.keyManager = keyManager;
	}

}
