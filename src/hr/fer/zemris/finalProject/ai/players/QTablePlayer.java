package hr.fer.zemris.finalProject.ai.players;

import hr.fer.zemris.finalProject.ai.Util;
import hr.fer.zemris.finalProject.game.GameState;
import hr.fer.zemris.finalProject.game.KeyManager;
import hr.fer.zemris.finalProject.game.Player;
import hr.fer.zemris.finalProject.game.frozenLake.FrozenLakeSimulator;

import java.io.InputStream;

public abstract class QTablePlayer implements GamePlayer {

	protected double[][] Q;

	private KeyManager keyManager;

	public QTablePlayer(InputStream playerStream) {
		Q = Util.loadTable(playerStream);
	}

	@Override
	public void gameStateChanged(GameState gameState) {
		Player player = gameState.getPlayer();
		int state = (int) (player.getY() * FrozenLakeSimulator.getMap()[0].length + player.getX());

		int nextAction = getNextAction(Q[state]);
		switch (nextAction) {
			case 0:
				keyManager.setKey(0);
				break;
			case 1:
				keyManager.setKey(1);
				break;
			case 2:
				keyManager.setKey(2);
				break;
			case 3:
				keyManager.setKey(3);
		}
	}

	private int getNextAction(double[] values) {
		double maximum = -Double.MAX_VALUE;
		int index = -1;
		for (int i = 0; i < values.length; i++) {
			if (values[i] > maximum) {
				maximum = values[i];
				index = i;
			}
		}

		return index;
	}

	@Override
	public void setKeyManager(KeyManager keyManager) {
		this.keyManager = keyManager;
	}
}