package hr.fer.zemris.finalProject.ai.players;

import hr.fer.zemris.finalProject.game.KeyManager;
import hr.fer.zemris.finalProject.game.listeners.GameChangedListener;

public interface GamePlayer extends GameChangedListener {

	void setKeyManager(KeyManager keyManager);
}