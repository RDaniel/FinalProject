package hr.fer.zemris.finalProject.game;

public interface Player {

	double getX();

	void setX(double x);

	double getY();

	void setY(double y);

	boolean isDead();

	boolean isWinner();

}