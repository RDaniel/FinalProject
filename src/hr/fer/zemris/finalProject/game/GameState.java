package hr.fer.zemris.finalProject.game;

public interface GameState {

	Player getPlayer();

	double[] getAIInputs();

}