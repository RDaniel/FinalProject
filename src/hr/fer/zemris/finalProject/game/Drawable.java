package hr.fer.zemris.finalProject.game;

import javafx.scene.canvas.GraphicsContext;

public interface Drawable {

	void draw(GraphicsContext graphicsContext, double width, double height);

}
