package hr.fer.zemris.finalProject.game.frozenLake;

import hr.fer.zemris.finalProject.game.AbstractGameSimulator;
import hr.fer.zemris.finalProject.game.GameState;
import hr.fer.zemris.finalProject.game.KeyManager;
import hr.fer.zemris.finalProject.game.flappy.util.RandomGenerator;
import hr.fer.zemris.finalProject.game.frozenLake.entities.FrozenPlayer;

import java.util.Arrays;

public class FrozenLakeSimulator extends AbstractGameSimulator {

	private static final int[][] MAP = new int[][]{
			{1, 1, 1, 1, 1, 1, 1, 2},
			{1, 1, -1, -1, -1, -1, 1, 1},
			{1, 1, 1, 1, 1, 1, 1, 1},
			{1, 1, 1, 1, 1, 1, 1, 1},
	};

	private final double windProbability;

	private final KeyManager keyManager;

	private FrozenPlayer player;

	private FrozenGameState gameState;

	public FrozenLakeSimulator(KeyManager keyManager, double simulatorStepTime, double windProbability) {
		super(simulatorStepTime);

		this.player = new FrozenPlayer();
		this.keyManager = keyManager;
		this.windProbability = windProbability;
	}

	public static int[][] getMap() {
		return MAP;
	}

	public GameState getGameState() {
		createGameState();
		return gameState;
	}

	@Override
	public void gameStep() {
		boolean[] keys = keyManager.getKeys();
		double randomDouble = RandomGenerator.getRandomDouble();
		if (randomDouble < windProbability) {
			Arrays.fill(keys, false);
			keys[RandomGenerator.getRandomInteger(0, keys.length - 1)] = true;
		}

		movePlayer(keys);
		if (MAP[player.y][player.x] == -1) {
			player.setDead(true);
		} else if (MAP[player.y][player.x] == 2) {
			player.setWinner(true);
		}

		createGameState();
		listeners.forEach(listener -> listener.gameStateChanged(gameState));
	}

	private void movePlayer(boolean[] keys) {
		if (keys[0] && player.y != 0) {
			player.y -= 1;
		} else if (keys[1] && player.y != MAP.length - 1) {
			player.y += 1;
		} else if (keys[2] && player.x != 0) {
			player.x -= 1;
		} else if (keys[3] && player.x != MAP[0].length - 1) {
			player.x += 1;
		}

		Arrays.fill(keys, false);
	}

	private void createGameState() {
		if (gameState == null) {
			gameState = new FrozenGameState(MAP);
		}

		gameState.setPlayer(player);
	}
}