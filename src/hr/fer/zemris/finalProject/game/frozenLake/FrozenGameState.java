package hr.fer.zemris.finalProject.game.frozenLake;

import hr.fer.zemris.finalProject.game.GameState;
import hr.fer.zemris.finalProject.game.Player;
import hr.fer.zemris.finalProject.game.frozenLake.entities.FrozenPlayer;

public class FrozenGameState implements GameState {

	private FrozenPlayer player;

	private int[][] map;

	public FrozenGameState(int[][] map) {
		this.map = map;
	}

	@Override
	public Player getPlayer() {
		return player;
	}

	public void setPlayer(FrozenPlayer player) {
		this.player = player;
	}

	@Override
	public double[] getAIInputs() {
		int width = map[0].length;
		int height = map.length;
		double[] aiInputs = new double[width * height];

		aiInputs[player.y * width + player.x] = 1;

		return aiInputs;
	}
}