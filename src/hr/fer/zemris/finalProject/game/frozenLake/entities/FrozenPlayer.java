package hr.fer.zemris.finalProject.game.frozenLake.entities;

import hr.fer.zemris.finalProject.game.Player;

public class FrozenPlayer implements Player {

	public int x;

	public int y;

	private boolean isDead;

	private boolean isWinner;

	@Override
	public double getX() {
		return x;
	}

	@Override
	public void setX(double x) {
		this.x = (int) x;
	}

	@Override
	public double getY() {
		return y;
	}

	@Override
	public void setY(double y) {
		this.y = (int) y;
	}

	@Override
	public boolean isDead() {
		return isDead;
	}

	public void setDead(boolean isDead) {
		this.isDead = isDead;
	}

	@Override
	public boolean isWinner() {
		return isWinner;
	}

	public void setWinner(boolean winner) {
		isWinner = winner;
	}
}
