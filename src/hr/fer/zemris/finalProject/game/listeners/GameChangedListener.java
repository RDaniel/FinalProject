package hr.fer.zemris.finalProject.game.listeners;

import hr.fer.zemris.finalProject.game.GameState;

public interface GameChangedListener {

	void gameStateChanged(GameState gameState);

}