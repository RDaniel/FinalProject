package hr.fer.zemris.finalProject.game;

/**
 * Represent key manager. Contains boolean array which indicates if some arrow key is pressed.
 * The order in next : up, down, left and right.
 */
public class KeyManager {

	private boolean[] keys;

	public KeyManager() {
		keys = new boolean[4];
	}

	public boolean[] getKeys() {
		return keys;
	}

	public void setKey(int key) {
		keys[key] = true;
	}
}