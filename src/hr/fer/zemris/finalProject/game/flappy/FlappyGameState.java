package hr.fer.zemris.finalProject.game.flappy;

import hr.fer.zemris.finalProject.game.GameState;
import hr.fer.zemris.finalProject.game.Player;
import hr.fer.zemris.finalProject.game.flappy.entities.FlappyPlayer;
import hr.fer.zemris.finalProject.game.flappy.entities.Pipe;

import java.util.List;

public class FlappyGameState implements GameState {

	private FlappyPlayer player;

	private List<Pipe> pipes;

	private boolean movingPipes;

	public FlappyGameState(boolean movingPipes) {
		this.movingPipes = movingPipes;
	}

	@Override
	public Player getPlayer() {
		return player;
	}

	public void setPlayer(FlappyPlayer player) {
		this.player = player;
	}

	public List<Pipe> getPipes() {
		return pipes;
	}

	public void setPipes(List<Pipe> pipes) {
		this.pipes = pipes;
	}

	@Override
	public double[] getAIInputs() {
		Pipe frontPipe = null;
		for (Pipe pipe : pipes) {
			if (pipe.getPosition() + Pipe.PIPE_WIDTH > player.getX()) {
				frontPipe = pipe;
				break;
			}
		}

		double pipeX = frontPipe.getPosition() + Pipe.PIPE_WIDTH;
		double pipeY = frontPipe.getHoleStart() - Pipe.HOLE_LENGTH;

		double[] aiState = new double[movingPipes ? 4 : 3];
		aiState[0] = pipeX - player.getX();
		aiState[1] = pipeY - player.getY();
		aiState[2] = player.getSpeed();
		if (movingPipes) {
			aiState[3] = frontPipe.getHoleMove() * 10;
		}

		return aiState;
	}
}
