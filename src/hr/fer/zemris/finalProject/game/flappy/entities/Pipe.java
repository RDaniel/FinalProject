package hr.fer.zemris.finalProject.game.flappy.entities;

public class Pipe {

	public static final int HOLE_LENGTH = 4;
	public static final int PIPE_WIDTH = 1;

	private double holeStart;

	private double position;

	private double holeMove;

	public Pipe(double holeStart) {
		this.holeStart = holeStart;
	}

	public double getHoleMove() {
		return holeMove;
	}

	public void setHoleMove(double holeMove) {
		this.holeMove = holeMove;
	}

	public double getHoleStart() {
		return holeStart;
	}

	public void setHoleStart(double holeStart) {
		this.holeStart = holeStart;
	}

	public double getPosition() {
		return position;
	}

	public void setPosition(double position) {
		this.position = position;
	}

	public void changeMoveDirection() {
		holeMove *= -1;
	}
}