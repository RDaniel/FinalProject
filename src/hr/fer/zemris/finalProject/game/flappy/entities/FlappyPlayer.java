package hr.fer.zemris.finalProject.game.flappy.entities;

import hr.fer.zemris.finalProject.game.Player;

public class FlappyPlayer implements Player {

	public static final int WIDTH = 1;

	public static final int HEIGHT = 1;

	public static final double MAX_Y_SPEED = 8.5;

	public static final double MIN_Y_SPEED = -6;

	private static final int X_POSITION = 2;

	private double yPosition = 8;

	private double speed = 0;

	private boolean dead;

	private double distancePassed;

	private int pipesPassed;

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public double getDistancePassed() {
		return distancePassed;
	}

	public void setDistancePassed(double distancePassed) {
		this.distancePassed = distancePassed;
	}

	@Override
	public double getX() {
		return X_POSITION;
	}

	@Override
	public void setX(double x) {
	}

	@Override
	public double getY() {
		return yPosition;
	}

	@Override
	public void setY(double y) {
		this.yPosition = y;
	}

	@Override
	public boolean isWinner() {
		return false;
	}

	@Override
	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
	}
}