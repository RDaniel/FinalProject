package hr.fer.zemris.finalProject.game.flappy.level;

import hr.fer.zemris.finalProject.game.flappy.GameWindow;
import hr.fer.zemris.finalProject.game.flappy.entities.Pipe;
import hr.fer.zemris.finalProject.game.flappy.util.RandomGenerator;

public class NormalLevelGenerator implements LevelGenerator {

	public static final int MINIMAL_HOLE_START = Pipe.HOLE_LENGTH + 1;
	public static final int MAXIMAL_HOLE_START = GameWindow.WINDOW_HEIGHT - 1;

	private static final int MAX_DIFFERENCE_BETWEEN_PIPES = 6;

	private Pipe previousPipe;

	@Override
	public Pipe getNextPipe() {
		double nextHoleStart = getNewPipeHoleStart();
		previousPipe = new Pipe(nextHoleStart);
		return previousPipe;
	}

	private double getNewPipeHoleStart() {
		if (previousPipe == null) {
			return RandomGenerator.getRandomInteger(MINIMAL_HOLE_START, MAXIMAL_HOLE_START);
		}

		double previousHoleStart = previousPipe.getHoleStart();
		double maxStart = previousHoleStart + MAX_DIFFERENCE_BETWEEN_PIPES;
		double minStart = previousHoleStart - MAX_DIFFERENCE_BETWEEN_PIPES;

		if (maxStart > MAXIMAL_HOLE_START) {
			maxStart = MAXIMAL_HOLE_START;
		}
		if (minStart < MINIMAL_HOLE_START) {
			minStart = MINIMAL_HOLE_START;
		}

		return RandomGenerator.getRandomDouble(minStart, maxStart);
	}
}