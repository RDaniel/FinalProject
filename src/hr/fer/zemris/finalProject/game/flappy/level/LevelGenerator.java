package hr.fer.zemris.finalProject.game.flappy.level;

import hr.fer.zemris.finalProject.game.flappy.entities.Pipe;

public interface LevelGenerator {

	Pipe getNextPipe();

}