package hr.fer.zemris.finalProject.game.flappy.level;

import hr.fer.zemris.finalProject.game.flappy.entities.Pipe;
import hr.fer.zemris.finalProject.game.flappy.util.RandomGenerator;

public class MovingPipesGenerator implements LevelGenerator {

	private LevelGenerator levelGenerator;

	public MovingPipesGenerator(LevelGenerator levelGenerator) {
		this.levelGenerator = levelGenerator;
	}

	@Override
	public Pipe getNextPipe() {
		Pipe pipe = levelGenerator.getNextPipe();
		int direction = RandomGenerator.getRandomBoolean() ? -1 : 1;
		double moveSpeed = (1 / (double) RandomGenerator.getRandomInteger(40, 80)) * direction;
		pipe.setHoleMove(moveSpeed);

		return pipe;
	}
}
