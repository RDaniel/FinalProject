package hr.fer.zemris.finalProject.game.flappy;

import hr.fer.zemris.finalProject.game.AbstractGameSimulator;
import hr.fer.zemris.finalProject.game.GameState;
import hr.fer.zemris.finalProject.game.KeyManager;
import hr.fer.zemris.finalProject.game.flappy.entities.FlappyPlayer;
import hr.fer.zemris.finalProject.game.flappy.entities.Pipe;
import hr.fer.zemris.finalProject.game.flappy.level.LevelGenerator;

import java.util.ArrayList;
import java.util.List;

/**
 * Game simulator for Flappy Birds.
 */
public class FlappySimulator extends AbstractGameSimulator {

	private static final double DEFAULT_SIMULATOR_STEP_TIME = 1 / 60.0;

	private static final double SIMULATOR_DT = 1 / 60.0;

	private static final double STEP_SHIFT = 2 / 60.0;

	private FlappyPlayer player;

	private KeyManager keyManager;

	private GameWindow gameWindow;

	private FlappyGameState currentGameState;

	private boolean movingPipes;

	public FlappySimulator(LevelGenerator levelGenerator, KeyManager keyManager, boolean movingPipes) {
		this(levelGenerator, keyManager, DEFAULT_SIMULATOR_STEP_TIME, movingPipes);
	}

	public FlappySimulator(LevelGenerator levelGenerator, KeyManager keyManager, double simulatorStepTime, boolean movingPipes) {
		super(simulatorStepTime);
		this.keyManager = keyManager;
		this.movingPipes = movingPipes;

		gameWindow = new GameWindow(levelGenerator, STEP_SHIFT);
		listeners = new ArrayList<>();
		player = new FlappyPlayer();
	}

	public GameState getGameState() {
		if (currentGameState == null) {
			createGameState();
		}

		return currentGameState;
	}

	public int getScore() {
		return gameWindow.getPassedPipes();
	}

	@Override
	public void gameStep() {
		gameWindow.move();

		player.setDistancePassed(player.getDistancePassed() + STEP_SHIFT);
		movePlayer();

		if (isInCollision()) {
			player.setDead(true);
		}

		createGameState();
		listeners.forEach(listener -> listener.gameStateChanged(currentGameState));
	}

	private void createGameState() {
		if (currentGameState == null) {
			currentGameState = new FlappyGameState(movingPipes);
		}

		currentGameState.setPlayer(player);
		currentGameState.setPipes(gameWindow.getWindowPipes());
	}

	private void movePlayer() {
		double speed = player.getSpeed();

		speed -= 0.3;
		if (keyManager.getKeys()[0]) {
			keyManager.getKeys()[0] = false;
			speed += FlappyPlayer.MAX_Y_SPEED;
		}

		if (speed > FlappyPlayer.MAX_Y_SPEED) {
			speed = FlappyPlayer.MAX_Y_SPEED;
		} else if (speed < FlappyPlayer.MIN_Y_SPEED) {
			speed = FlappyPlayer.MIN_Y_SPEED;
		}

		player.setSpeed(speed);

		double move = speed * SIMULATOR_DT;
		player.setY(player.getY() + move);

		if (player.getY() > GameWindow.WINDOW_HEIGHT || player.getY() - FlappyPlayer.HEIGHT < 0) {
			player.setDead(true);
		}
	}

	private boolean isInCollision() {
		List<Pipe> currentPipes = gameWindow.getWindowPipes();

		for (Pipe currentPipe : currentPipes) {
			double pipePosition = currentPipe.getPosition();
			if (pipePosition < player.getX() + FlappyPlayer.WIDTH && pipePosition + Pipe.PIPE_WIDTH > player.getX()) {

				double holeStart = currentPipe.getHoleStart();
				if (player.getY() > holeStart || player.getY() - FlappyPlayer.HEIGHT < holeStart - Pipe.HOLE_LENGTH) {
					return true;
				}

				break;
			}
		}

		return false;
	}
}