package hr.fer.zemris.finalProject.game.flappy;

import hr.fer.zemris.finalProject.game.flappy.entities.Pipe;
import hr.fer.zemris.finalProject.game.flappy.level.LevelGenerator;
import hr.fer.zemris.finalProject.game.flappy.level.NormalLevelGenerator;

import java.util.LinkedList;
import java.util.List;

public class GameWindow {

	public static final int WINDOW_WIDTH = 8;
	public static final int WINDOW_HEIGHT = 12;
	public static final double GROUND_PERCENT = 0.2;

	private static final int NEXT_PIPE_BORDER = 4;

	private final double STEP_SHIFT;

	private double currentShift;

	private List<Pipe> windowPipes = new LinkedList<>();

	private LevelGenerator levelGenerator;

	private int passedPipes;

	public GameWindow(LevelGenerator levelGenerator, double stepShift) {
		this.levelGenerator = levelGenerator;
		this.STEP_SHIFT = stepShift;

		windowPipes.add(generateNewPipe());
	}

	public int getPassedPipes() {
		return passedPipes;
	}

	public List<Pipe> getWindowPipes() {
		return windowPipes;
	}

	public void move() {
		shiftPipes();

		currentShift += STEP_SHIFT;

		if (currentShift >= NEXT_PIPE_BORDER) {
			currentShift = 0;
			Pipe nextPipe = generateNewPipe();
			windowPipes.add(nextPipe);
		}
	}

	private void shiftPipes() {
		boolean removeFirst = false;
		for (Pipe pipe : windowPipes) {
			pipe.setPosition(pipe.getPosition() - STEP_SHIFT);
			if (pipe.getPosition() + Pipe.PIPE_WIDTH <= 0) {
				removeFirst = true;
			}
			pipe.setHoleStart(pipe.getHoleStart() + pipe.getHoleMove());
			if (pipe.getHoleStart() > NormalLevelGenerator.MAXIMAL_HOLE_START) {
				pipe.setHoleStart(NormalLevelGenerator.MAXIMAL_HOLE_START);
				pipe.changeMoveDirection();
			} else if (pipe.getHoleStart() < NormalLevelGenerator.MINIMAL_HOLE_START) {
				pipe.setHoleStart(NormalLevelGenerator.MINIMAL_HOLE_START);
				pipe.changeMoveDirection();
			}
		}

		if (removeFirst) {
			passedPipes++;
			windowPipes.remove(0);
		}
	}

	private Pipe generateNewPipe() {
		Pipe nextPipe = levelGenerator.getNextPipe();
		nextPipe.setPosition(WINDOW_WIDTH);

		return nextPipe;
	}
}