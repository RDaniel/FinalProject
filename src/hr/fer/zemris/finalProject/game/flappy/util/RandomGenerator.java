package hr.fer.zemris.finalProject.game.flappy.util;

import java.util.concurrent.ThreadLocalRandom;

public class RandomGenerator {

	private static final ThreadLocalRandom randomGenerator = ThreadLocalRandom.current();

	public static int getRandomInteger() {
		return randomGenerator.nextInt();
	}

	public static double getGaussian() {
		return randomGenerator.nextGaussian();
	}

	/**
	 * Generates random integer in given range. Borders are included.
	 *
	 * @param min minimum border
	 * @param max maximum border
	 * @return random integer
	 */
	public static int getRandomInteger(int min, int max) {
		return randomGenerator.nextInt(max - min + 1) + min;
	}

	public static double getRandomDouble() {
		return randomGenerator.nextDouble();
	}

	public static boolean getRandomBoolean() {
		return randomGenerator.nextBoolean();
	}

	/**
	 * Generates random double in given range. Borders are included.
	 *
	 * @param min minimum border
	 * @param max maximum border
	 * @return random double
	 */
	public static double getRandomDouble(double min, double max) {
		return randomGenerator.nextDouble() * (max - min) + min;
	}

}