package hr.fer.zemris.finalProject.game;

import hr.fer.zemris.finalProject.game.listeners.GameChangedListener;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractGameSimulator {

	private final long simulatorStepTimeInMilliseconds;

	protected List<GameChangedListener> listeners;

	private boolean running;

	public AbstractGameSimulator(double simulatorStepTime) {
		this.simulatorStepTimeInMilliseconds = (long) (simulatorStepTime * 1000);

		listeners = new ArrayList<>();
	}

	public synchronized void start() {
		if (running) {
			return;
		}

		running = true;
		long currentTime;
		long lastTime = System.currentTimeMillis();

		while (running) {
			if (simulatorStepTimeInMilliseconds > 0) {
				currentTime = System.currentTimeMillis();
				long targetTime = lastTime + simulatorStepTimeInMilliseconds;
				long timeToWait = targetTime - currentTime;

				while (timeToWait > 0) {
					try {
						wait(timeToWait);
					} catch (InterruptedException ignored) {
					}
					currentTime = System.currentTimeMillis();
					timeToWait = targetTime - currentTime;
				}

				lastTime = System.currentTimeMillis();
			}

			gameStep();
		}
	}

	public abstract void gameStep();

	public void stop() {
		running = false;
	}

	public void addStateChangedListener(GameChangedListener gameChangedListener) {
		listeners.add(gameChangedListener);
	}
}
