package hr.fer.zemris.finalProject.gui.listeners;

import hr.fer.zemris.finalProject.ai.SolutionListener;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * A solution listener used for displaying the iteration-fitness values on
 * a graph, once the algorithm has completed.
 */
public class FitnessPlotter extends Application implements SolutionListener {

	/**
	 * Holds the series which stores the values to be plotted on the graph.
	 */
	private static XYChart.Series<Number, Number> series = new XYChart.Series<>();

	public static void main(String[] args) {
		loadData(args[0]);
		launch(args);
	}

	private static void loadData(String data) {
		Path path = Paths.get(data);
		List<String> dataValues = null;
		try {
			dataValues = Files.readAllLines(path);
		} catch (IOException ignored) {
		}
		List<XYChart.Data<Number, Number>> temp = new ArrayList<>();

		int zeroCounter = 0;
		final int zeroBorder = 100;
		for (String dataValue : dataValues) {
			String[] values = dataValue.split("\\s+");
			double fitness = Double.parseDouble(values[1]);
			if (fitness == 0.0) {
				if (zeroCounter++ % zeroBorder == 0) {
					temp.add(new XYChart.Data<>(Integer.parseInt(values[0]), fitness));
				}
			} else {
				temp.add(new XYChart.Data<>(Integer.parseInt(values[0]), fitness));
			}
		}

		series.getData().addAll(temp);
	}

	public static void displayPlot(String data) {
		loadData(data);

		Platform.runLater(() -> {
			Stage stage = new Stage();
			stage.setResizable(false);
			stage.setTitle("Fitness Plot");

			showPlot(stage);
		});
	}

	private static void showPlot(Stage stage) {
		NumberAxis xAxis = new NumberAxis();
		NumberAxis yAxis = new NumberAxis();
		xAxis.setLabel("Iteration");
		yAxis.setLabel("Score");

		ScatterChart<Number, Number> lineChart = new ScatterChart<>(xAxis, yAxis);
		lineChart.getXAxis().setTickMarkVisible(true);
		lineChart.getXAxis().setTickLabelsVisible(true);
		series.setName("Q-Function value plotter");

		Scene scene = new Scene(lineChart, 800, 600);
		lineChart.getData().add(series);

		stage.setScene(scene);
		stage.show();
	}

	@Override
	public void start(Stage stage) {
		showPlot(stage);
	}

	@Override
	public void solutionGenerated(long iteration, double solutionFitness) {
		Platform.runLater(() -> series.getData().add(new XYChart.Data<>(iteration, solutionFitness)));
	}
}