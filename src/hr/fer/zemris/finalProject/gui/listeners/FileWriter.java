package hr.fer.zemris.finalProject.gui.listeners;

import hr.fer.zemris.finalProject.ai.SolutionListener;
import hr.fer.zemris.finalProject.gui.Constants;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;


public class FileWriter implements SolutionListener {

	private List<DataPair> dataPairs = new ArrayList<>();

	private Path filePath;

	private long maxIteration;

	public FileWriter(long maxIteration) {
		this.maxIteration = maxIteration;

		try {
			Files.createDirectories(Paths.get(Constants.FITNESS_DATA_DIR));
		} catch (IOException ignored) {
		}

		filePath = Paths.get(Constants.FITNESS_DATA_DIR + File.separator + Constants.FITNESS_FILE);
		try {
			Files.deleteIfExists(filePath);
		} catch (IOException ignored) {
		}
	}

	@Override
	public void solutionGenerated(long iteration, double solutionFitness) {
		dataPairs.add(new DataPair(iteration, solutionFitness));

		if (iteration == maxIteration - 1) {
			writeDataToFile();
		}
	}

	private void writeDataToFile() {
		try (BufferedWriter writer = Files.newBufferedWriter(filePath, StandardOpenOption.CREATE)) {
			StringBuilder builder = new StringBuilder();
			dataPairs.forEach(dataPair -> builder.append(dataPair).append(System.lineSeparator()));
			writer.write(builder.toString());
			writer.flush();
			dataPairs.clear();
		} catch (IOException ignored) {
		}
	}

	private static class DataPair {
		long iteration;
		double solutionFitness;

		DataPair(long iteration, double solutionFitness) {
			this.iteration = iteration;
			this.solutionFitness = solutionFitness;
		}

		@Override
		public String toString() {
			return iteration + " " + solutionFitness;
		}
	}
}