package hr.fer.zemris.finalProject.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ApplicationStarter extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		StageHolder.getInstance().setStage(stage);

		stage.setTitle("RL for Automatic Game Play");

		Parent root = FXMLLoader.load(ApplicationStarter.class.getClassLoader().getResource("fxml/menu.fxml"));
		Scene primaryScene = new Scene(root);
		stage.setScene(primaryScene);

		stage.show();
	}
}