package hr.fer.zemris.finalProject.gui.controllers;

import hr.fer.zemris.finalProject.ai.Util;
import hr.fer.zemris.finalProject.ai.players.frozen.FrozenQNetworkPlayer;
import hr.fer.zemris.finalProject.ai.players.frozen.FrozenQTablePlayer;
import hr.fer.zemris.finalProject.gui.GUIUtil;
import hr.fer.zemris.finalProject.gui.GameRunner;
import hr.fer.zemris.finalProject.gui.runners.FrozenGameRunner;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.InputStream;

import static hr.fer.zemris.finalProject.gui.Constants.*;

public class FrozenController {

	private static final String Q_TABLE = "qt";
	private static final String S_TABLE = "st";
	private static final String NEURAL_NETWORK = "nn";

	private static final String DEFAULT_PLAYER = "defaultPlayer";

	@FXML
	TextField windProbability;

	@FXML
	ToggleGroup players;

	public void playListener(ActionEvent event) {
		Stage stage = new Stage();
		stage.setResizable(false);
		stage.setTitle("Frozen Lake");

		GameRunner gameRunner;
		try {
			gameRunner = getGameRunner(((Button) event.getSource()).getId());
		} catch (Exception e) {
			GUIUtil.showError();
			return;
		}
		Scene scene = gameRunner.run();

		stage.setScene(scene);
		stage.show();
	}

	public void trainingListener() throws IOException {
		ControllerUtil.changeScene("frozenTrainer.fxml");
	}

	private GameRunner getGameRunner(String modelID) {
		RadioButton selectedButton = (RadioButton) players.getSelectedToggle();
		boolean defaultPlayer = selectedButton.getId().equals(DEFAULT_PLAYER);
		double wind = Double.parseDouble(windProbability.getText());

		InputStream player;
		switch (modelID) {
			case Q_TABLE:
				player = defaultPlayer ? Util.getDefaultPlayer(Q_TABLE_PLAYER) : Util.getCustomPlayer(Q_TABLE_PLAYER);
				return new FrozenGameRunner(new FrozenQTablePlayer(player), wind);
			case S_TABLE:
				player = defaultPlayer ? Util.getDefaultPlayer(SARSA_PLAYER) : Util.getCustomPlayer(SARSA_PLAYER);
				return new FrozenGameRunner(new FrozenQTablePlayer(player), wind);
			case NEURAL_NETWORK:
				player = defaultPlayer ? Util.getDefaultPlayer(Q_NETWORK_PLAYER) : Util.getCustomPlayer(Q_NETWORK_PLAYER);
				return new FrozenGameRunner(new FrozenQNetworkPlayer(player), wind);
		}

		throw new IllegalArgumentException("Invalid button ID!");
	}

	public void backListener() throws Exception {
		ControllerUtil.changeScene("menu.fxml");
	}
}