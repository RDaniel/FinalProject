package hr.fer.zemris.finalProject.gui.controllers;

import hr.fer.zemris.finalProject.ai.*;
import hr.fer.zemris.finalProject.game.GameState;
import hr.fer.zemris.finalProject.game.frozenLake.FrozenLakeSimulator;
import hr.fer.zemris.finalProject.gui.Constants;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FrozenTrainingController implements SolutionListener {

	private static final long NUMBER_OF_GAME_ITERATIONS = 50;

	private static final int UPDATE_BORDER = 100;

	private static final Path Q_LEARNING_TABLE_FILE = Paths.get(Constants.CUSTOM_ROOT_DIR + File.separator + Constants.Q_TABLE_PLAYER);
	private static final Path Q_LEARNING_NN_FILE = Paths.get(Constants.CUSTOM_ROOT_DIR + File.separator + Constants.Q_NETWORK_PLAYER);
	private static final Path SARSA_TABLE_FILE = Paths.get(Constants.CUSTOM_ROOT_DIR + File.separator + Constants.SARSA_PLAYER);

	private static final String Q_LEARNING_TABLE = "qLearningTable";
	private static final String SARSA_TABLE = "sarsaTable";
	private static final String Q_LEARNING_NN = "qLearningNN";

	@FXML
	private ToggleGroup algorithms;

	@FXML
	private TextField numberOfIterationsInput;

	@FXML
	private ProgressBar progressBar;

	@FXML
	private TextField windProbabilityInput;

	@FXML
	private TextField epsilonProbabilityInput;

	@FXML
	private TextField discountFactorInput;

	@FXML
	private TextField learningRateInput;

	private int numberOfIterations;
	private double windProbability;
	private double epsilonProbability;
	private double discountFactor;
	private double learningRate;
	private int counter;

	@FXML
	void startListener() throws IOException {
		Files.createDirectories(Paths.get(Constants.PLAYERS_DIR));
		Platform.runLater(() -> progressBar.setProgress(0));

		numberOfIterations = Integer.parseInt(numberOfIterationsInput.getText());
		windProbability = Double.parseDouble(windProbabilityInput.getText());
		epsilonProbability = Double.parseDouble(epsilonProbabilityInput.getText());
		learningRate = Double.parseDouble(learningRateInput.getText());
		discountFactor = Double.parseDouble(discountFactorInput.getText());

		RadioButton selectedButton = (RadioButton) algorithms.getSelectedToggle();
		switch (selectedButton.getId()) {
			case Q_LEARNING_TABLE:
				qLearningTable();
				break;
			case SARSA_TABLE:
				sarsaTale();
				break;
			case Q_LEARNING_NN:
				qLearningWithNeuralNetwork();
				break;
		}
	}

	@FXML
	void backListener() throws IOException {
		ControllerUtil.changeScene("frozen.fxml");
	}

	@Override
	public void solutionGenerated(long iteration, double solutionFitness) {
		if (++counter != UPDATE_BORDER) {
			return;
		}

		counter = 0;
		Platform.runLater(() -> progressBar.setProgress(iteration / (double) numberOfIterations));
	}

	private void qLearningTable() {
		QTableLearner tableLearner = new QTableLearner(new FrozenEnvironment<>(state ->
				(int) (state.getPlayer().getY() * FrozenLakeSimulator.getMap()[0].length + state.getPlayer().getX()
				), windProbability),
				numberOfIterations,
				epsilonProbability,
				learningRate,
				discountFactor);
		tableLearner.addSolutionListener(this);

		Thread workerThread = new Thread(() -> {
			tableLearner.learn();
			Util.saveTable(tableLearner.getQ(), Q_LEARNING_TABLE_FILE);
		});

		workerThread.start();
	}

	private void qLearningWithNeuralNetwork() {
		QNeuralNetworkLearner neuralNetworkLearner = new QNeuralNetworkLearner(new FrozenEnvironment<>(GameState::getAIInputs, windProbability),
				false,
				false,
				numberOfIterations,
				NUMBER_OF_GAME_ITERATIONS,
				epsilonProbability,
				discountFactor,
				learningRate);
		neuralNetworkLearner.addSolutionListener(this);

		Thread workerThread = new Thread(() -> Util.saveNetwork(Q_LEARNING_NN_FILE, neuralNetworkLearner.learn()));
		workerThread.start();
	}

	private void sarsaTale() {
		SARSATableLearner tableLearner = new SARSATableLearner(new FrozenEnvironment<>(state ->
				(int) (state.getPlayer().getY() * FrozenLakeSimulator.getMap()[0].length + state.getPlayer().getX()
				), windProbability),
				numberOfIterations,
				epsilonProbability,
				learningRate,
				discountFactor);
		tableLearner.addSolutionListener(this);

		Thread workerThread = new Thread(() -> {
			tableLearner.learn();
			Util.saveTable(tableLearner.getQ(), SARSA_TABLE_FILE);
		});

		workerThread.start();
	}
}