package hr.fer.zemris.finalProject.gui.controllers;

import hr.fer.zemris.finalProject.gui.ApplicationStarter;
import hr.fer.zemris.finalProject.gui.StageHolder;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

public class ControllerUtil {

	public static void changeScene(String sceneName) throws IOException {
		Stage stage = StageHolder.getInstance().getStage();
		Parent root = FXMLLoader.load(ApplicationStarter.class.getClassLoader().getResource("fxml" + File.separator + sceneName));
		Scene scene = new Scene(root);

		stage.setScene(scene);
		stage.show();
	}
}
