package hr.fer.zemris.finalProject.gui.controllers;

import hr.fer.zemris.finalProject.ai.FlappyEnvironment;
import hr.fer.zemris.finalProject.ai.QNeuralNetworkLearner;
import hr.fer.zemris.finalProject.ai.SolutionListener;
import hr.fer.zemris.finalProject.ai.Util;
import hr.fer.zemris.finalProject.game.GameState;
import hr.fer.zemris.finalProject.gui.Constants;
import hr.fer.zemris.finalProject.gui.listeners.FileWriter;
import hr.fer.zemris.finalProject.gui.listeners.FitnessPlotter;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FlappyTrainingController implements SolutionListener {

	private static final Path STATIC_PIPES_FILE = Paths.get(Constants.CUSTOM_ROOT_DIR + File.separator + Constants.STATIC_PLAYER);
	private static final Path MOVING_PIPES_FILE = Paths.get(Constants.CUSTOM_ROOT_DIR + File.separator + Constants.MOVING_PLAYER);

	private static final long NUMBER_OF_GAME_ITERATIONS = Long.MAX_VALUE;

	@FXML
	private CheckBox movingPipesInput;

	@FXML
	private CheckBox experienceReplay;

	@FXML
	private CheckBox targetNetwork;

	@FXML
	private Label maximalRewardInput;

	@FXML
	private TextField learningRateInput;

	@FXML
	private ProgressBar progressBar;

	@FXML
	private TextField numberOfIterationsInput;

	@FXML
	private TextField discountFactorInput;

	@FXML
	private TextField epsilonProbabilityInput;

	private int numberOfIterations;

	private double maximalReward = -Double.MAX_VALUE;

	@FXML
	void startListener() throws IOException {
		Files.createDirectories(Paths.get(Constants.PLAYERS_DIR));

		Platform.runLater(() -> progressBar.setProgress(0));
		Platform.runLater(() -> maximalRewardInput.setText(""));
		maximalReward = 0;

		numberOfIterations = Integer.parseInt(numberOfIterationsInput.getText());
		double epsilonProbability = Double.parseDouble(epsilonProbabilityInput.getText());
		double learningRate = Double.parseDouble(learningRateInput.getText());
		double discountFactor = Double.parseDouble(discountFactorInput.getText());

		boolean movingPipes = movingPipesInput.isSelected();

		QNeuralNetworkLearner neuralNetworkLearner = new QNeuralNetworkLearner(new FlappyEnvironment<>(GameState::getAIInputs, movingPipes),
				experienceReplay.isSelected(),
				targetNetwork.isSelected(),
				numberOfIterations,
				NUMBER_OF_GAME_ITERATIONS,
				epsilonProbability,
				discountFactor,
				learningRate);
		neuralNetworkLearner.addSolutionListener(this);
		neuralNetworkLearner.addSolutionListener(new FileWriter(numberOfIterations));

		Thread workerThread = new Thread(() -> {
			Util.saveNetwork(movingPipes ? MOVING_PIPES_FILE : STATIC_PIPES_FILE, neuralNetworkLearner.learn());

			FitnessPlotter.displayPlot(Constants.FITNESS_DATA_DIR + File.separator + Constants.FITNESS_FILE);
		});

		workerThread.start();
	}

	@FXML
	void backListener() throws IOException {
		ControllerUtil.changeScene("flappy.fxml");
	}

	@Override
	public void solutionGenerated(long iteration, double solutionFitness) {
		Platform.runLater(() -> progressBar.setProgress(iteration / (double) numberOfIterations));

		if (solutionFitness > maximalReward) {
			maximalReward = solutionFitness;
			Platform.runLater(() -> maximalRewardInput.setText(String.format("%.2f", maximalReward)));
		}
	}
}