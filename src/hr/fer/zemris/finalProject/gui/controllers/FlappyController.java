package hr.fer.zemris.finalProject.gui.controllers;

import hr.fer.zemris.finalProject.ai.Util;
import hr.fer.zemris.finalProject.ai.players.flappy.FlappyQNetworkPlayer;
import hr.fer.zemris.finalProject.gui.Constants;
import hr.fer.zemris.finalProject.gui.GUIUtil;
import hr.fer.zemris.finalProject.gui.GameRunner;
import hr.fer.zemris.finalProject.gui.runners.FlappyGameRunner;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.InputStream;

public class FlappyController {

	private static final String HUMAN_PLAYER = "human";
	private static final String NETWORK_PLAYER = "qnn";
	private static final String DEFAULT_PLAYER = "defaultPlayer";

	@FXML
	private CheckBox movingPipes;

	@FXML
	private ToggleGroup players;

	public void playListener(ActionEvent event) {
		Stage stage = new Stage();

		GameRunner gameRunner;
		try {
			gameRunner = getGameRunner(((Button) event.getSource()).getId());
		} catch (Exception e) {
			System.out.println(e.toString());
			GUIUtil.showError();
			return;
		}

		Scene scene = gameRunner.run();

		stage.setScene(scene);
		stage.show();
	}

	public void backListener() throws Exception {
		ControllerUtil.changeScene("menu.fxml");
	}

	public void trainingListener() throws IOException {
		ControllerUtil.changeScene("flappyTrainer.fxml");
	}

	private GameRunner getGameRunner(String id) {
		boolean movePipes = movingPipes.isSelected();

		switch (id) {
			case HUMAN_PLAYER:
				return new FlappyGameRunner(movePipes);
			case NETWORK_PLAYER:
				RadioButton selectedButton = (RadioButton) players.getSelectedToggle();
				boolean defaultPlayer = selectedButton.getId().equals(DEFAULT_PLAYER);

				InputStream player;
				if (defaultPlayer) {
					player = movePipes ? Util.getDefaultPlayer(Constants.MOVING_PLAYER) : Util.getDefaultPlayer(Constants.STATIC_PLAYER);
				} else {
					player = movePipes ? Util.getCustomPlayer(Constants.MOVING_PLAYER) : Util.getCustomPlayer(Constants.STATIC_PLAYER);
				}

				return new FlappyGameRunner(new FlappyQNetworkPlayer(player), movePipes);
		}

		throw new IllegalArgumentException("Invalid button ID!");
	}
}