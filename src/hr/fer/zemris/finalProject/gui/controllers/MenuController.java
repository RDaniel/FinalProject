package hr.fer.zemris.finalProject.gui.controllers;

import java.io.IOException;

public class MenuController {

	public void flappyBirdListener() throws IOException {
		ControllerUtil.changeScene("flappy.fxml");
	}

	public void frozenLakeListener() throws Exception {
		ControllerUtil.changeScene("frozen.fxml");
	}

	public void exitActionListener() {
		System.exit(0);
	}
}