package hr.fer.zemris.finalProject.gui;

import hr.fer.zemris.finalProject.ai.players.flappy.FlappyGamePlayer;
import hr.fer.zemris.finalProject.game.GameState;
import hr.fer.zemris.finalProject.game.flappy.FlappyGameState;
import hr.fer.zemris.finalProject.game.flappy.GameWindow;
import hr.fer.zemris.finalProject.game.flappy.entities.FlappyPlayer;
import hr.fer.zemris.finalProject.game.flappy.entities.Pipe;
import hr.fer.zemris.finalProject.game.listeners.GameChangedListener;
import javafx.application.Platform;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import java.util.List;
import java.util.Objects;

public class FlappyDisplay extends Canvas implements GameChangedListener {

	private final FlappyGamePlayer gamePlayer;

	private Image birdImage;
	private Image groundImage;
	private Image backgroundImage;
	private Image topPipeImage;
	private Image bottomPipeImage;

	private List<Pipe> pipes;
	private FlappyPlayer player;

	private int jumpHappened;

	public FlappyDisplay(FlappyGamePlayer gamePlayer, int width, int height) {
		super(width, height);

		this.gamePlayer = gamePlayer;
	}

	private void paintComponent(GameState gameState) {
		if (Objects.isNull(gameState)) {
			return;
		}

		FlappyGameState state = (FlappyGameState) gameState;
		pipes = state.getPipes();
		player = (FlappyPlayer) state.getPlayer();

		GraphicsContext graphicsContext = getGraphicsContext2D();

		double width = getWidth();
		double height = getHeight();

		draw(graphicsContext, width, height);

		if (gamePlayer != null) {
			drawQValues(graphicsContext, width, height, gamePlayer.getQValues(gameState.getAIInputs()));
		}
	}

	private void drawQValues(GraphicsContext graphicsContext, double width, double height, double[] qValues) {
		boolean jump = qValues[0] > qValues[1];
		if (jump) {
			jumpHappened = 5;
		} else {
			jumpHappened--;
		}

		jump = jump || jumpHappened > 0;
		graphicsContext.setFont(Font.font("Arial", FontWeight.BOLD, 25));
		FontMetrics fm = new FontMetrics(graphicsContext.getFont());

		String text = "DON'T JUMP = " + String.format("%.2f", qValues[1]);
		float textWidth = fm.getWidth(text);
		graphicsContext.setFill(jump ? Color.GREEN : Color.RED);
		graphicsContext.fillText("JUMP = " + String.format("%.2f", qValues[0]), width / 2 - textWidth / 2, height - 10 - fm.getHeight());
		graphicsContext.setFill(jump ? Color.RED : Color.GREEN);
		graphicsContext.fillText(text, width / 2 - textWidth / 2, height - 5);
	}

	private void draw(GraphicsContext graphicsContext, double width, double height) {
		if (backgroundImage == null) {
			loadImages();
		}

		int groundBorder = (int) (height - height * GameWindow.GROUND_PERCENT);

		double xPart = width / GameWindow.WINDOW_WIDTH;
		double yPart = groundBorder / GameWindow.WINDOW_HEIGHT;

		drawBackground(graphicsContext, groundBorder, width, height);
		drawPlayer(graphicsContext, groundBorder, xPart, yPart);
		drawPipes(graphicsContext, groundBorder, xPart, yPart);
	}

	private void drawBackground(GraphicsContext graphicsContext, int groundBorder, double width, double height) {
		graphicsContext.drawImage(backgroundImage,
				0,
				0,
				width,
				groundBorder);

		graphicsContext.drawImage(groundImage,
				0,
				groundBorder,
				width,
				height - groundBorder);

	}

	private void drawPlayer(GraphicsContext graphicsContext, int groundBorder, double xPart, double yPart) {
		graphicsContext.drawImage(
				birdImage,
				(int) (player.getX() * xPart),
				(int) (groundBorder - player.getY() * yPart),
				(int) (FlappyPlayer.WIDTH * xPart),
				(int) (FlappyPlayer.HEIGHT * yPart)
		);
	}

	private void drawPipes(GraphicsContext graphicsContext, int groundBorder, double xPart, double yPart) {
		pipes.forEach(pipe -> {
			graphicsContext.drawImage(
					topPipeImage,
					(int) (pipe.getPosition() * xPart),
					0,
					(int) (Pipe.PIPE_WIDTH * xPart),
					(int) (groundBorder - pipe.getHoleStart() * yPart)
			);
			graphicsContext.drawImage(
					bottomPipeImage,
					(int) (pipe.getPosition() * xPart),
					(int) (groundBorder - (pipe.getHoleStart() - Pipe.HOLE_LENGTH) * yPart),
					(int) (Pipe.PIPE_WIDTH * xPart),
					(int) ((pipe.getHoleStart() - Pipe.HOLE_LENGTH) * yPart)
			);
		});
	}

	@Override
	public void gameStateChanged(GameState gameState) {
		Platform.runLater(() -> paintComponent(gameState));
	}

	private void loadImages() {
		birdImage = new Image(Pipe.class.getClassLoader().getResource("bird.png").toString());
		groundImage = new Image(Pipe.class.getClassLoader().getResource("ground.png").toString());
		backgroundImage = new Image(Pipe.class.getClassLoader().getResource("bg.png").toString());
		topPipeImage = new Image(Pipe.class.getClassLoader().getResource("tube1.png").toString());
		bottomPipeImage = new Image(Pipe.class.getClassLoader().getResource("tube2.png").toString());
	}
}