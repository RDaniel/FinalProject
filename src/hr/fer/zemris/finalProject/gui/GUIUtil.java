package hr.fer.zemris.finalProject.gui;

import javafx.scene.control.Alert;
import javafx.scene.layout.Region;
import javafx.stage.StageStyle;

public class GUIUtil {

	public static void showError() {
		Alert alert = new Alert(Alert.AlertType.ERROR);
		alert.initStyle(StageStyle.UNIFIED);
		alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
		alert.setTitle("Exception");
		alert.setContentText("Could not create game runner! Check if player definition exists!");

		alert.showAndWait();
	}

}