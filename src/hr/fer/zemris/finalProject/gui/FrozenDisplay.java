package hr.fer.zemris.finalProject.gui;

import hr.fer.zemris.finalProject.game.GameState;
import hr.fer.zemris.finalProject.game.Player;
import hr.fer.zemris.finalProject.game.frozenLake.FrozenGameState;
import hr.fer.zemris.finalProject.game.frozenLake.FrozenLakeSimulator;
import hr.fer.zemris.finalProject.game.listeners.GameChangedListener;
import javafx.application.Platform;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import java.util.Objects;

public class FrozenDisplay extends Canvas implements GameChangedListener {

	private final int[][] map;
	private final double[] QValues;

	private final int mapWidth;
	private final int mapHeight;

	private Image playerImage;
	private Image dangerImage;
	private Image surfaceImage;
	private Image goalImage;

	public FrozenDisplay(double[] QValues, int width, int height) {
		super(width, height);
		map = FrozenLakeSimulator.getMap();
		mapWidth = map[0].length;
		mapHeight = map.length;

		this.QValues = QValues;
	}

	private void paintComponent(GameState gameState) {
		if (Objects.isNull(gameState)) {
			return;
		}

		GraphicsContext graphicsContext = getGraphicsContext2D();

		double width = getWidth();
		double height = getHeight();

		double xPart = width / mapWidth;
		double yPart = height / mapHeight;

		draw(graphicsContext, xPart, yPart, gameState.getPlayer());
		drawQValues(graphicsContext, xPart, yPart);
	}

	private void draw(GraphicsContext graphicsContext, double xPart, double yPart, Player player) {
		if (playerImage == null) {
			loadImages();
		}

		for (int i = 0; i < mapHeight; i++) {
			for (int j = 0; j < mapWidth; j++) {
				graphicsContext.drawImage(surfaceImage, xPart * j, yPart * i, xPart, yPart);
				graphicsContext.strokeRect(xPart * j, yPart * i, xPart, yPart);

				if (player.getY() == i && player.getX() == j) {
					graphicsContext.drawImage(playerImage, xPart * j, yPart * i, xPart, yPart);
				} else if (map[i][j] == 2) {
					graphicsContext.drawImage(goalImage, xPart * j, yPart * i, xPart, yPart);
				} else if (map[i][j] == -1) {
					graphicsContext.drawImage(dangerImage, xPart * j, yPart * i, xPart, yPart);
				}
			}
		}
	}

	private void drawQValues(GraphicsContext graphicsContext, double xPart, double yPart) {
		graphicsContext.setFont(Font.font(Font.getDefault().getFamily(), FontWeight.BOLD, Font.getDefault().getSize()));
		FontMetrics fm = new FontMetrics(graphicsContext.getFont());

		for (int i = 0, j = 0; i < QValues.length; i += 4, j++) {
			double max = -Double.MAX_VALUE;
			String top = String.format("%.1f", QValues[i]);
			max = QValues[i] > max ? QValues[i] : max;
			String bottom = String.format("%.1f", QValues[i + 1]);
			max = QValues[i + 1] > max ? QValues[i + 1] : max;
			String left = String.format("%.1f", QValues[i + 2]);
			max = QValues[i + 2] > max ? QValues[i + 2] : max;
			String right = String.format("%.1f", QValues[i + 3]);
			max = QValues[i + 3] > max ? QValues[i + 3] : max;

			double x = (j % mapWidth) * xPart;
			double y = (j / mapWidth) * yPart;

			String maxValue = String.format("%.1f", max);
			graphicsContext.setFill(Color.RED);
			graphicsContext.fillText(maxValue, x + xPart / 2 - fm.getWidth(maxValue) / 2, y + yPart / 2.0f + fm.getHeight() / 2.0f);

			graphicsContext.setFill(Color.BLUE);
			graphicsContext.fillText(top, x + xPart / 2.0f - fm.getWidth(top) / 2.0f, y + fm.getHeight() + 3);
			graphicsContext.fillText(bottom, x + xPart / 2.0f - fm.getWidth(bottom) / 2.0f, y + yPart - 3);
			graphicsContext.fillText(left, x + 3, y + yPart / 2.0f + fm.getHeight() / 2.0f);
			graphicsContext.fillText(right, x + xPart - fm.getWidth(right) - 3, y + yPart / 2.0f + fm.getHeight() / 2.0f);
		}
	}

	@Override
	public void gameStateChanged(GameState gameState) {
		Platform.runLater(() -> paintComponent(gameState));
	}

	private void loadImages() {
		playerImage = new Image(FrozenGameState.class.getClassLoader().getResource("dolphin.png").toString());
		dangerImage = new Image(FrozenGameState.class.getClassLoader().getResource("danger.jpeg").toString());
		surfaceImage = new Image(FrozenGameState.class.getClassLoader().getResource("ice.jpg").toString());
		goalImage = new Image(FrozenGameState.class.getClassLoader().getResource("goal.png").toString());
	}
}