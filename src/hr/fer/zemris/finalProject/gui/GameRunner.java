package hr.fer.zemris.finalProject.gui;

import javafx.scene.Scene;

public interface GameRunner {

	Scene run();

}