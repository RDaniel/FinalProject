package hr.fer.zemris.finalProject.gui;

import javafx.geometry.Bounds;
import javafx.scene.text.Font;
import javafx.scene.text.Text;


public class FontMetrics {
	private final Text internal;
	private float lineHeight;

	public FontMetrics(Font fnt) {
		internal = new Text();
		internal.setFont(fnt);
		Bounds b = internal.getLayoutBounds();
		lineHeight = (float) b.getHeight();
	}

	public float getWidth(String txt) {
		internal.setText(txt);
		return (float) internal.getLayoutBounds().getWidth();
	}

	public float getHeight() {
		return lineHeight;
	}
}