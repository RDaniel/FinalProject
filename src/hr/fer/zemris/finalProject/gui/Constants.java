package hr.fer.zemris.finalProject.gui;

import java.io.File;

public class Constants {

	public static final String FITNESS_DATA_DIR = "rlData" + File.separator + "data";
	public static final String FITNESS_FILE = "fitness.data";

	public static final String PLAYERS_DIR = "rlData" + File.separator + "players";
	public static final String CUSTOM_ROOT_DIR = "rlData";

	public static final String Q_TABLE_PLAYER = "players" + File.separator + "FrozenQ.table";
	public static final String Q_NETWORK_PLAYER = "players" + File.separator + "FrozenNetwork.net";
	public static final String SARSA_PLAYER = "players" + File.separator + "FrozenS.table";

	public static final String STATIC_PLAYER = "players" + File.separator + "FlappyNetwork.net";
	public static final String MOVING_PLAYER = "players" + File.separator + "FlappyMovingNetwork.net";
}
