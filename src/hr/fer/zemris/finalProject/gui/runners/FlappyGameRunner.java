package hr.fer.zemris.finalProject.gui.runners;

import hr.fer.zemris.finalProject.ai.players.flappy.FlappyGamePlayer;
import hr.fer.zemris.finalProject.game.GameState;
import hr.fer.zemris.finalProject.game.KeyManager;
import hr.fer.zemris.finalProject.game.flappy.FlappySimulator;
import hr.fer.zemris.finalProject.game.flappy.level.LevelGenerator;
import hr.fer.zemris.finalProject.game.flappy.level.MovingPipesGenerator;
import hr.fer.zemris.finalProject.game.flappy.level.NormalLevelGenerator;
import hr.fer.zemris.finalProject.game.listeners.GameChangedListener;
import hr.fer.zemris.finalProject.gui.FlappyDisplay;
import hr.fer.zemris.finalProject.gui.GameRunner;
import javafx.scene.Group;
import javafx.scene.Scene;

public class FlappyGameRunner implements GameRunner, GameChangedListener {

	private FlappySimulator flappySimulator;

	private KeyManager keyManager;

	private boolean humanPlayer;

	private FlappyGamePlayer gamePlayer;

	private FlappyDisplay flappyDisplay;

	private boolean movePipes;

	public FlappyGameRunner(boolean pipes) {
		this.humanPlayer = true;
		this.movePipes = pipes;
		keyManager = new KeyManager();
	}

	public FlappyGameRunner(FlappyGamePlayer gamePlayer, boolean movePipes) {
		this.gamePlayer = gamePlayer;
		this.movePipes = movePipes;
		keyManager = new KeyManager();
	}

	@Override
	public Scene run() {
		flappyDisplay = new FlappyDisplay(gamePlayer, 450, 650);
		Group root = new Group();
		root.getChildren().add(flappyDisplay);

		Scene scene = new Scene(root);
		if (humanPlayer) {
			scene.setOnMouseClicked(e -> keyManager.getKeys()[0] = true);
		} else {
			gamePlayer.setKeyManager(keyManager);
		}

		startGame();

		return scene;
	}

	private void startGame() {
		initializeGameSimulator();
		Thread workerThread = new Thread(() -> flappySimulator.start());
		workerThread.start();
	}

	@Override
	public void gameStateChanged(GameState gameState) {
		if (gameState.getPlayer().isDead()) {
			System.out.println(flappySimulator.getScore());
			flappySimulator.stop();
			initializeGameSimulator();
			flappySimulator.start();
		}
	}

	private void initializeGameSimulator() {
		LevelGenerator levelGenerator;
		if (movePipes) {
			levelGenerator = new MovingPipesGenerator(new NormalLevelGenerator());
		} else {
			levelGenerator = new NormalLevelGenerator();
		}

		flappySimulator = new FlappySimulator(levelGenerator, keyManager, movePipes);
		flappySimulator.addStateChangedListener(this);
		flappySimulator.addStateChangedListener(flappyDisplay);

		if (!humanPlayer) {
			flappySimulator.addStateChangedListener(gamePlayer);
		}
	}
}