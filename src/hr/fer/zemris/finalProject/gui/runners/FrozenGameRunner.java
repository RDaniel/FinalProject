package hr.fer.zemris.finalProject.gui.runners;

import hr.fer.zemris.finalProject.ai.players.frozen.FrozenGamePlayer;
import hr.fer.zemris.finalProject.game.GameState;
import hr.fer.zemris.finalProject.game.KeyManager;
import hr.fer.zemris.finalProject.game.frozenLake.FrozenLakeSimulator;
import hr.fer.zemris.finalProject.game.listeners.GameChangedListener;
import hr.fer.zemris.finalProject.gui.FrozenDisplay;
import hr.fer.zemris.finalProject.gui.GameRunner;
import javafx.scene.Group;
import javafx.scene.Scene;

public class FrozenGameRunner implements GameRunner, GameChangedListener {

	private final double windProbability;

	private FrozenDisplay frozenDisplay;

	private FrozenLakeSimulator frozenSimulator;

	private FrozenGamePlayer player;

	public FrozenGameRunner(FrozenGamePlayer player, double windProbability) {
		this.player = player;
		this.windProbability = windProbability;
	}

	@Override
	public Scene run() {
		frozenDisplay = new FrozenDisplay(player.getQValues(), 1200, 800);
		Group root = new Group();
		root.getChildren().add(frozenDisplay);

		Scene scene = new Scene(root);
		startGame();

		return scene;
	}

	private void startGame() {
		KeyManager keyManager = new KeyManager();
		player.setKeyManager(keyManager);
		frozenSimulator = new FrozenLakeSimulator(keyManager, 1, windProbability);
		frozenSimulator.addStateChangedListener(this);
		frozenSimulator.addStateChangedListener(player);
		frozenSimulator.addStateChangedListener(frozenDisplay);

		GameState gameState = frozenSimulator.getGameState();
		gameStateChanged(gameState);

		Thread workerThread = new Thread(() -> frozenSimulator.start());
		workerThread.start();
	}

	@Override
	public void gameStateChanged(GameState gameState) {
		if (gameState.getPlayer().isDead()) {
			System.out.println("Game over!");
			frozenSimulator.stop();
		} else if (gameState.getPlayer().isWinner()) {
			System.out.println("Winner!");
			frozenSimulator.stop();
		}
	}
}