package hr.fer.zemris.finalProject.gui;

import javafx.stage.Stage;

public class StageHolder {

	private static StageHolder ourInstance = new StageHolder();
	private Stage stage;

	private StageHolder() {
	}

	public static StageHolder getInstance() {
		return ourInstance;
	}

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}
}
